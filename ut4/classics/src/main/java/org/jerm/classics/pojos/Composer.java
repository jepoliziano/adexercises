package org.jerm.classics.pojos;

import org.bson.types.ObjectId;
import org.bson.codecs.pojo.annotations.*;

import java.util.List;
import java.util.ArrayList;


public class Composer {

    //
    // ------- Atributos -------
    //
    @BsonId
    private ObjectId _id;

    @BsonProperty(value = "name")
    private String name;

    @BsonProperty(value = "alias")
    private String alias;

    private List<Work> works;


    //
    // ------- Constructores -------
    //

    //Constructor por defecto
    public Composer(){
        this.works = new ArrayList<>();
    };

    //Constructor general, para especificar el nombre y el sobrenombre del compositor.
    public Composer(String name, String alias){
        this.name = name;
        this.alias = alias;
        this.works = new ArrayList<>();
    }

    //Constructor general para especificar el valor de todos los atributos de instancia
    public Composer(String name, String alias, List<Work> works){
        this.name = name;
        this.alias = alias;
        this.works = works;
    }


    //
    // ------- Métodos 'getter' y 'setter' -------
    //
    public ObjectId get_id(){ return this._id; }

    public String getName(){ return this.name; }
    public void setName(String name){ this.name = name; }

    public String getAlias(){ return this.alias; }
    public void setAlias(String alias){ this.alias = alias; }

    public List<Work> getWorks(){ return this.works; }
    public void setWorks(List<Work> works){ this.works = works; }


    //
    // ------- Otros métodos -------
    //

    //Sobrescritura del metodo toString() de la clase Object.
    @Override
    public String toString() {
        return "Composer{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                ", alias='" + alias + '\'' +
                ", works=" + works +
                '}';
    }

    //Añadir una instancia Work a la colección de obras de este compositor.
    public void addWork(Work work){
        this.works.add(work);
    }
}
