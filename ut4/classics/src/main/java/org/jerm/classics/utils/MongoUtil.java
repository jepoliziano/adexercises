package org.jerm.classics.utils;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.jerm.classics.pojos.Composer;

import java.util.Properties;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

// Esta clase, implementando el patrón 'creacional' Singleton, define un único método
// estático que devuelve la colección de objetos Composer sobre la que opera la aplicación.
public class MongoUtil {

    // Constantes para alcanzar la instancia MongoCollection<>.
    final private static ConnectionString connectionString;
    final private static CodecRegistry pojoCodecRegistry;
    final private static CodecRegistry codecRegistry;
    final private static MongoClientSettings mongoClientSettings;
    final private static MongoClient mongoClient;
    final private static MongoDatabase classicsDB;
    final private static MongoCollection<Composer> composersCollection;

    // Inicializador estático para instanciar la colección
    // de compositores de que se hará uso en la aplicación.
    static {
        connectionString = new ConnectionString("mongodb://localhost");
        pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        mongoClientSettings = MongoClientSettings.builder()
                                                 .applyConnectionString(connectionString)
                                                 .codecRegistry(codecRegistry)
                                                 .build();
        mongoClient = MongoClients.create(mongoClientSettings);
        classicsDB = mongoClient.getDatabase("classics");
        composersCollection = classicsDB.getCollection("composers", Composer.class);
    }

    //Constructor privado del Singleton.
    private MongoUtil() { }

    //Método estático para obtener la colección de compositores.
    public static MongoCollection<Composer> getComposersCollection() {
        return composersCollection;
    }

    //Cerrar la instancia MongoClient.
    public static void closeMongoClient(){
        mongoClient.close();
    }
}

