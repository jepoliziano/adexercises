package org.jerm.classics;

import org.jerm.classics.services.ComposerService;
import org.jerm.classics.views.ConsoleView;

import static org.jerm.classics.utils.MongoUtil.closeMongoClient;
import static org.jerm.classics.utils.MongoUtil.getComposersCollection;

public class Main {
    public static void main(String[]rgs) throws InterruptedException{

        // Objetos asociados al patrón MVC.
        ConsoleView view = new ConsoleView();;
        ComposerService composerService = new ComposerService();
        Controller controller = new Controller(view, composerService);

        // Estas dos primeras instrucciones pretenden evitar, respectivamente, que
        // los mensajes que arroja la consola al establecer la conexión con la base
        // de datos interfieran con a interfaz de la aplicación y que esos mismos
        // mensajes se entremezclen con el menú que se muestra al inicio del programa.
        getComposersCollection();
        Thread.sleep(70);
        controller.controllerMain();
        closeMongoClient();
    }
}
