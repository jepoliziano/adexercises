package org.jerm.classics;

import org.jerm.classics.pojos.Composer;
import org.jerm.classics.pojos.Work;
import org.jerm.classics.services.ComposerService;
import org.jerm.classics.services.WorkService;
import org.jerm.classics.views.ConsoleView;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Controller {

    //
    //------- Atributos -------
    //

    private ConsoleView view;
    private Composer composer;
    private Work work;
    private ComposerService composerService;
    private Scanner input;
    private int option; // Elección del usuario cada vez se le indica que elija una opción.


    //
    //------- Constructores -------
    //

    // Constructor general
    public Controller(ConsoleView view, ComposerService composerService) {
        this.view = view;
        this.composerService = composerService;
        this.input = new Scanner(System.in);
    }


    //
    //------- Métodos -------
    //

    //Método que recrea el comportamiento propio del método main() de una clase ejecutable.
    public void controllerMain() {

        while(true){

            // Limpiar la consola cada iteración del bucle.
            this.view.clearView();

            this.view.showMainMenu();
            this.view.showChooseOptionMessage(4, true);
            this.option = input.nextInt();

            // Control de errores: el usuario es instado a seleccionar una opción válida (1,2,3,4,0).
            while(this.option < 0 || this.option > 4){
                this.view.showWrongOptionMessage(this.option, false);
                this.view.showChooseOptionMessage(4, true);
                this.option = input.nextInt();
            }


            switch(this.option){

                // Crear un nuevo o varios nuevos documentos.
                case 1:
                    insertComposers();
                    break;

                // Modificar documento.
                case 2:
                    break;

                // Eliminar documento.
                case 3:
                    break;

                // Consultar documento.
                case 4:
                    break;

                // Abandonar la aplicación.
                case 0:
                    System.out.println();
                    System.exit(0);
            }

            this.view.showStayMessage();
            this.option = (int)input.next().toLowerCase().charAt(0);

            // Control de errores: el usuario es instado a seleccionar una opción válida (s,n).
            while((int)'s' != this.option && (int)'n' != this.option){
                this.view.showWrongOptionMessage(this.option, true);
                this.view.showStayMessage();
                this.option = (int)input.next().toLowerCase().charAt(0);
            }

            // Dentro del ámbito de este 'if', se da por terminada la ejecución del programa.
            if((int)'n'== this.option){
                System.out.println();
                System.exit(0);
            }
        }
    }

    // Op.1) Crear uno o varios nuevos documentos.
    private void insertComposers() {

        String name, alias;
        List<Work> works;
        ArrayList<Composer> composers = new ArrayList<>();
        long totalInsertions;
        boolean composersMatch;

        while (true) {

            this.view.clearView();
            this.view.showTitle("\nregistro de nuevo compositor");

            this.view.showInsertDataMessage("Nombre");
            input.nextLine(); //Vaciado del buffer del teclado.
            name = input.nextLine();

            this.view.showInsertDataMessage("Sobrenombre");
            alias = input.nextLine();

            works = setWorkList();

            // Eliminación de duplicados: comprobación de que
            // no se introduzca ningún compositor repetido.
            this.composer = new Composer(name, alias, works);
            composersMatch = false;

            for (int i = 0; i < composers.size() && !composersMatch; i++)
                if(this.composer.getAlias().compareToIgnoreCase(composers.get(i).getAlias()) == 0)
                    composersMatch = true;

            if(!composersMatch)
                composers.add(this.composer);

            this.view.showYesOrNoMessage("Desea registrar otro compositor");
            this.option = input.next().toLowerCase().charAt(0);

            while ((int)'s' != this.option && (int)'n' != this.option) {
                this.view.showWrongOptionMessage(this.option, true);
                this.view.showYesOrNoMessage("Desea registrar otro compositor");
                this.option = input.next().toLowerCase().charAt(0);
            }

            if ((int)'n' == this.option)
                break;
        }

        if((totalInsertions = this.composerService.insertComposer(composers)) > 0)
            this.view.showInsertionSucceededMessage(totalInsertions);
        else
            this.view.showInsertionFailedMessage();
    }


    // Tanto al crear como al modificar un compositor, el usuario
    // ha de poder especificar las obras asociadas al mismo.
    private List<Work> setWorkList(){

        List<Work> works = new ArrayList<>();
        this.work = null;
        String title, musicalForm;
        int version;
        boolean worksMatch;

        this.view.showYesOrNoMessage("Desea agregar alguna obra a este compositor");
        this.option = input.next().toLowerCase().charAt(0);

        // Repetición del bucle mientras el usuario desee incorporar obras al compositor.
        while(true) {

            while ((int)'s' != this.option && (int)'n' != this.option) {
                this.view.showWrongOptionMessage(this.option, true);
                this.view.showYesOrNoMessage("Desea agregar alguna otra obra a este compositor");
                this.option = input.next().toLowerCase().charAt(0);
            }

            if ((int)'n' == this.option)
                break;

            this.view.showInsertDataMessage("\nTítulo de la obra");
            input.nextLine(); //Vaciado del buffer del teclado.
            title = input.nextLine();

            this.view.showInsertDataMessage("Forma musical");
            musicalForm = input.nextLine();

            this.view.showInsertDataMessage("Versión");
            version = input.nextInt();

            // Eliminación de duplicados: comprobación
            // de que no se haya repetido ninguna obra.
            this.work = new Work(title, musicalForm, version);
            worksMatch = false;

            for (int i = 0; i <works.size() && !worksMatch; i++)
                if(WorkService.checkMatchingWorks(this.work, works.get(i)))
                    worksMatch = true;

            if(!worksMatch)
                works.add(this.work);

            this.view.showYesOrNoMessage("Desea agregar alguna otra obra a este compositor");
            this.option = input.next().toLowerCase().charAt(0);
        }

        return works;
    }
}