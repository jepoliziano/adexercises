package org.jerm.classics.services;

import org.jerm.classics.pojos.Work;

public class WorkService {

    //Comprobar si los datos de las dos instancias Work recibidas por parámetro coinciden.
    public static boolean checkMatchingWorks(Work work1, Work work2){
        return checkMatchingTitles(work1.getTitle(), work2.getTitle()) &&
               checkMatchingMusicalForms(work1.getMusicalForm(), work2.getMusicalForm()) &&
               checkMatchingVersions(work1.getVersion(), work2.getVersion());
    }

    //Comprobar si los dos títulos recibidos por parámetro coinciden.
    public static boolean checkMatchingTitles(String title1, String title2){

        return (title1.compareToIgnoreCase(title2) == 0);
    }

    //Comprobar si el nombre de las dos formas musicales recibidas por parámetro coinciden.
    public static boolean checkMatchingMusicalForms(String musicalForm1, String musicalForm2){
        return (musicalForm1.compareToIgnoreCase(musicalForm2) == 0);
    }

    //Comprobar si las dos cifras correspondientes a versiones recibidas por parámetro coinciden.
    public static boolean checkMatchingVersions(int version1, int version2){
        return version1 == version2;
    }
}
