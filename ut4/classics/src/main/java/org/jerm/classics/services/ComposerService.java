package org.jerm.classics.services;


import com.mongodb.client.model.InsertManyOptions;
import org.jerm.classics.pojos.Composer;
import org.jerm.classics.pojos.Work;

import static org.jerm.classics.utils.MongoUtil.getComposersCollection;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;
import static com.mongodb.client.model.Projections.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Vector;


public class ComposerService {

    //------- Métodos de instancia -------

    //Agregar un nuevo registro a la tabla de clientes.
    public long insertComposer(List<Composer> newComposers){

        ArrayList<Composer> filterComposers = new ArrayList<>();
        long documentCountBeforeInsertion = getDocumentCount();

        // Eliminación de duplicados. La lista dinámica filterComposers será el resultado
        // de eliminar los compositores repetidos de la lista recibida por parámetro.
        newComposers.forEach(composer -> {
            if(searchComposer(composer.getAlias()) == null)
                filterComposers.add(composer);
        });

        // Esta condición evita la excepción de tipo MongoBulkWriteException que
        // se produce cuando el método insertMany() se invoca sobre una lista vacía.
        if (!filterComposers.isEmpty())
            getComposersCollection().insertMany(filterComposers, new InsertManyOptions().ordered(false));

        return getDocumentCount() - documentCountBeforeInsertion;
    }

    //
    //------- Métodos de estáticos -------
    //

    // Localizar un compositor en la BBDD.
    public static Composer searchComposer(String alias){
        return getComposersCollection().find(eq("alias", alias)).first();
    }

    // Obtener el numero total de documentos de la coleccion.
    public static long getDocumentCount(){
        return getComposersCollection().countDocuments();
    }
}
