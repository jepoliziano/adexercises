package org.jerm.classics.pojos;

import org.bson.codecs.pojo.annotations.*;

public class Work {

    //
    // ------- Atributos -------
    //
    @BsonProperty(value = "title")
    private String title;

    @BsonProperty(value = "musical_form")
    private String musicalForm;

    @BsonProperty(value = "version")
    private int version;


    //
    // ------- Constructores -------
    //

    //Constructor por defecto
    public Work(){}

    //Constructor general, para especificar el valor de todos los atributos de la instancia
    public Work(String title, String musicalForm, int version){
        this.title = title;
        this.musicalForm = musicalForm;
        this.version = version;
    }


    //
    // ------- Metodos 'getter' y 'setter' -------
    //

    public String getTitle(){ return this.title; }
    public void setTitle(String title){ this.title = title; }

    public String getMusicalForm(){ return this.musicalForm; }
    public void setMusicalForm(String musicalForm){ this.musicalForm = musicalForm; }

    public int getVersion(){ return this.version; }
    public void setVersion(int version){ this.version = version; }


    //
    // ------- Otros métodos -------
    //

    //Sobrescritura del metodo toString() de la clase Object.
    @Override
    public String toString() {
        return "Work{" +
                "title='" + title + '\'' +
                ", musicalForm='" + musicalForm + '\'' +
                ", version=" + version +
                '}';
    }
}
