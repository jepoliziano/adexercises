DESCRIPCIÓN GENERAL
-------------------

	Implementando un patrón de diseño MVC, este programa se sustenta sobre una base de datos MongoDB con una única colección, cada uno de cuyos documentos define un compositor, que, entre otros, cuenta con un campo complejo para almacenar las obras asociadas al mismo. Ambos compositor y obras están representados por sendas clases POJO, mapeadas como documentos MongoDB.
	Extendiendo los requisitos de la práctica, el proyecto, entre otros:
	- Define una clase Singleton para establecer la conexión con el servidor de Mongo.
	- Se ejceuta de forma dinámica, deleganddo en el usuario la decisión de qué datos introducir en la colección.
	- Implementa mecanismos para prevenir la inserción de datos duplicados.
	- Realiza consultas sobre la base de datos que implementan proyecciones y filtros avanzados.
