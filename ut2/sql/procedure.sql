CREATE OR REPLACE PROCEDURE PUBLIC.charge_maintanance_expenses() LANGUAGE plpgsql
AS $procedure$
BEGIN
	UPDATE account SET account_balance = account_balance - 25;
END;
$procedure$;
