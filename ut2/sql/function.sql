CREATE OR REPLACE FUNCTION PUBLIC.count_clients_richer_than(amount REAL)  
RETURNS INT  
LANGUAGE plpgsql  
AS $function$  
DECLARE q INT; 
BEGIN  
	SELECT COUNT(account_client) INTO q FROM account WHERE account_balance > amount; 
RETURN q;  
END; 
$function$;