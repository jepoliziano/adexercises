README
======

	i) Esta carpeta contiene un conjunto de clases definidas en el contexto del paquete 'jdbc' que ponen a prueba los conocimientos estudiados en esta unidad, en relación con la interacción entre Java y los motores de bases de datos. Las clases están concebidas para ser ejecutadas como un todo desde MainClass.class, que contiene el método 'main()', a lo largo de cuya ejecución, el usuario recibe información de las operaciones que están teniendo lugar en cada momento, después cada una de las cuales, el programa suspende su ejecución el tiempo requerido por el usuario para dar la oportunidad de verificar los cambios que se efectúan sobre la base de datos.
	Para conocer los detalles de la funcionalidad de cada una de las clases que conforman este paquete, consultar la documentación que las acompaña, que se puede generar con el comando indicado abajo.
	ii) Antes de ejecutar MainClass.class, definir la variable 'ip' declarada en la línea 36 de MainClass.java según corresponda.


COMANDOS ÚTILES
---------------

NOTA: los tres comandos a continuación deben lanzarse desde el directorio raiz del proyecto, en cuyo primer nivel, inicialmente, se encuentran los subdirectorios 'jars', 'sql', 'src', y el archivo de texto plano 'readme.txt'.

i) Generar la documentación del paquete:
	$ javadoc -noqualifier all --source-path ./src -cp ./jars/commons-dbcp2-2.9.0.jar:./jars/commons-dbutils-1.7.jar:./bin -package jdbc -d ./doc -author

ii) Comando de compilación:
	$ javac -cp ./jars/commons-dbcp2-2.9.0.jar:./jars/commons-dbutils-1.7.jar -d ./bin ./src/jdbc/*.java

iii) Comando de ejecución:
	$ java -cp ./jars/postgresql-42.2.23.jar:./jars/commons-dbutils-1.7.jar:./jars/commons-dbcp2-2.9.0.jar:./jars/commons-pool2-2.11.1.jar:./jars/commons-logging-1.2.jar:./bin jdbc.MainClass
