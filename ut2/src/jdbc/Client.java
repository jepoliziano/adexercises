
package jdbc;

import java.sql.*;
import java.text.SimpleDateFormat;




/**
 * POJO que representa los clientes de la base de datos concebida para este paquete de clases. Los objetos
 * <em>Client</em> están identificados con los atributos necesarios para manejar la información propia de
 * cualquier cliente de una entidad bancaria a través de los métodos que define la clase.
 */
public class Client{


	//------- Atributos -------
	private String dni;
	private String name;
	private String surname;
	private Date birthdate;
	private String email;



	//------- Constructores -------

	/**
	 * Constructor general para definir el valor de todos los atributos con que se identifica al titular de la
	 * cuenta bancaria, representados todos con cadenas de caracteres.
	 * 
	 * @param dni DNI del titular de la cuenta.
	 * <p>
	 *	El DNI cosiste en una cadena formada por un número de ocho dígitos y una letra. La clase <em>Client</em>
	 *  contiene los mecanismos necesarios para validar un DNI. En este sentido, basta con introducir la parte
	 *	numérica; la letra se generará automáticamente, así como no importa la longitud de la cifra introducida;
	 *	solo se tendrán en cuenta los ocho primeros números de la cadena para componer el DNI.
	 * </p>
	 * @param name Nombre del titular de la cuenta.
	 * @param surname Apellido del titular de la cuenta.
	 * @param birthdate Fecha de nacimiento del titular de la cuenta.
	 * @param email Correo electrónico del titular de la cuenta.
	 */
	public Client(String dni, String name, String surname, String birthdate, String email){
		this.dni = this.generateDNI(dni);
		this.name = name;
		this.surname = surname;
		this.birthdate = Date.valueOf(birthdate);
		this.email = email;
	}

	/**
	 * Constructor de copia, que construye una nueva instancia de la clase replicando la información de los
	 * atributos del objeto recibido como parámetro.
	 * 
	 * @param c Objeto <em>Client</em> a copiar.
	 */
	public Client(Client c){
		this.dni = c.dni;
		this.name = c.name;
		this.surname = c.surname;
		this.birthdate = c.birthdate;
		this.email = c.email;
	}



	//------- Métodos 'setter' y 'getter' -------

	/**
	 * Establecer el DNI del titular de la cuenta, en las mismas condiciones descritas en el contructor general.
	 * 
	 * @param dni Cadena de texto que almacena el DNI del titular de la cuenta o cualquier cadena formada por un
	 * número de ocho cifras o más a partir de la cual se generará el DNI. 
	 */
	public void setDNI(String dni){ this.dni = generateDNI(dni); }

	/**
	 * Obtener el DNI del titular de la cuenta.
	 * 
	 * @return El dni del titular de la cuenta.
	 */
	public String getDNI(){ return this.dni; }


	/**
	 * Definir el nombre del titular de la cuenta.
	 * 
	 * @param name String con el nombre del titular de la cuenta.
	 */
	public void setName(String name){ this.name = name; }

	/**
	 * Obtener el nombre del titular de la cuenta.
	 * 
	 * @return String con el nombre del titular de la cuenta.
	 */
	public String getName(){ return this.name; }


	/**
	 * Definir el apellido del titular de la cuenta.
	 * 
	 * @param surname Apellido del titular de la cuenta.
	 */
	public void setSurname(String surname){ this.surname = surname; }

	/**
	 * Obtener el apellido del titular de la cuenta.
	 * 
	 * @return Cadena de caracteres con el apellido del titular de la cuenta.
	 */
	public String getSurname(){ return this.surname; }


	/**
	 * Fijar la fecha de nacimiento del titular de la cuenta.
	 * 
	 * @param birthdate Fecha de nacimiento del titular de la cuenta en forma de String, que el método se
	 * encargará de adecuar al formato de la base de datos para almacenar fechas.
	 */
	public void setBirthdate(String birthdate){ this.birthdate = Date.valueOf(birthdate); }

	/**
	 * Obtener la fecha de nacimiento del titular de la cuenta.
	 * 
	 * @return Fecha de nacimiento del titular de la cuenta, como instnacia <em>Date</em>.
	 */
	public Date getBirthdate(){ return this.birthdate; }


	/**
	 * Establecer el correo electrónico del titular de la cuenta.
	 * 
	 * @param email Cadena de caracteres con el correo electrónico del titular de la cuenta.
	 */
	public void setEmail(String email){ this.email = email; }

	/**
	 * Obtener el correo electrónico del titular de la cuenta.
	 * 
	 * @return Cadena de caracteres que almacena el correo electrónico del titular de la cuenta.
	 */
	public String getEmail(){ return this.email; }



	//------- Métodos adicionales -------

	//Método para generar/validar el NIF a partir del DNI recibido como parámetro.
	private String generateDNI(String dni){

		int position;
		char letter;
		String lettersSequence = "TRWAGMYFPDXBNJZSQVHLCKE";
		
		dni = dni.substring(0,8);
		position = Integer.parseInt(dni) % 23;
		letter = lettersSequence.charAt(position);
		dni += letter;
		
		return dni;
	}


	/**
	 * Sobrescritura del método toString() definido en Object para mostrar por consola las características
	 * de la instancia actual.
	 * 
	 * @return Una cadena de caracteres para imprimir en consola un párrafo de texto que enumera el valor
	 * de los atributos del cliente actual.
	 */
	@Override public String toString(){

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String birthdateStr = dateFormat.format(this.birthdate);

		return "DNI: " + Format.CYAN_COLOUR + this.dni + Format.RESET_FORMAT + "\n" +
			   "Nombre: " + this.name + "\n" +
			   "Apellidos: " + this.surname + "\n" +
			   "Fecha de nacimiento: " + birthdateStr + "\n" +
			   "E-mail: " + this.email;
	}


	/**
	 * A partir de un vector de Strings, este método construye un objeto <em>Client</em>, interpretando
	 * secuencialmente cada uno de los elementos del vector como un atributo de la instancia.
	 * 
	 * @param clientAttributes Conjunto de atributos que identifican la instancia <em>Client</em> actual,
	 * almacenados como elementos de un vector de cadenas.
	 * 
	 * @return El objeto <em>Client</em> construido a partir del array de Strings, cada uno de cuyos
	 * elementos se convierte en un atributo de la instancia, en el orden en que figuran en el vector
	 * recibido.
	 */
	public static Client toClient(String[] clientAttributes){

		String dni = clientAttributes[0],
			   name = clientAttributes[1],
			   surname = clientAttributes[2],
			   birthdate = clientAttributes[3],
			   email = clientAttributes[4];

		return new Client(dni, name, surname, birthdate, email);
	}
}
