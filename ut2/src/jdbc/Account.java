/**
 * El paquete <em>jdbc</em> ha sido concebido para realizar operaciones que involucran una base de datos,
 * implementando, para ello, las clases e interfaces que Java proporciona a tal fin.
 */
package jdbc;




/**
 * La información que almacenará la base de datos ideada como parte del paquete <em>jdbc</em> procede de
 * instancias <em>Account</em> y <em>Client</em>, clases, ambas dos, creadas de forma paralela a las tablas
 * de la base de datos y cuyos atributos se corresponden con los campos de estas. Se trata, simplemente, de
 * ejercitar los mecanismos que caracterizan al paradigma de Programación Orientada a Objetos en que encaja
 * Java. En este sentido, <em>Account</em> y <em>Client</em> constituyen lo que en Java se conoce como
 * <em>POJO</em> o <em>Plain Old  Java Object</em>, una clase que no extiende ni implementa ninguna otra,
 * sino que puede considerse independiente.
 * Para el caso, <em>Account</em> representa una cuenta bancaria del mundo real y define los miembros necesarios
 * para manejar la información propia de la misma.
 * 
 * @author Juan Elías Risueño Martínez
 */
public class Account{
	

	//------- Atributos -------

	//Atributos asociados a las instancias de la clase
	private String clientDNI;
	private String iban;
	private double balance;

	//Atributos estáticos
	private static String countryCode;
	private static String entity;

	static{
		countryCode = "ES";
		entity = "3445";
	}



	//------- Constructores -------

	/**
	 * Constructor general, que genera un IBAN para la cuenta aleatorimamente.
	 * 
	 * @param clientDNI Cadena que almacena el DNI del titular de la cuenta, registrado en la base de datos.
	 * @param balance Valor real que indica el saldo de la cuenta.
	 */
	/*
	  NOTA: este constructor solo puede ser utilizado en el momento en que se registra una nueva cuenta. Para
	  que el IBAN de la cuenta coincida con el que se generó en el momento de sus constitución, a la hora de
	  listar los datos, hay que recurrir al segundo constructor parametrizado.
	*/
	public Account(String clientDNI, double balance){
		this.clientDNI = clientDNI;
		this.iban = this.generateIBAN();
		this.balance = balance;
	}

	/**
	 * Constructor parametrizado para especificar todos los datos de la instancia.
	 * 
	 * @param iban Número IBAN de la cuenta bancaria.
	 * @param clientDNI Cadena que almacena el DNI del titular de la cuenta, registrado en la base de datos.
	 * @param balance Valor real que indica el saldo de la cuenta.
	 */
	public Account(String iban, String clientDNI, double balance){
		this.iban = iban;
		this.clientDNI = clientDNI;
		this.balance = balance;
	}

	/**
	 * Constructor de copia.
	 * 
	 * @param a Instancia <em>Account</em> a partir de cuyos atributos se creará el nuevo objeto.
	 */
	public Account(Account a){
		this.clientDNI = a.clientDNI;
		this.iban = a.iban;
		this.balance = a.balance;
	}
	


	//-------  Métodos 'setter' y 'getter' -------
	
	/**
	 * Establecer el valor del atributo que almacena el DNI.
	 * 
	 * @param clientDNI Cadena que almacena el DNI del titular de la cuenta, registrado en la base de datos.
	 */
	public void setClientDNI(String clientDNI){ this.clientDNI = clientDNI; }

	/**
	 * Obtener la cadena que almacena el DNI del titular de la cuenta.
	 * 
	 * @return El valor de la cadena con el DNI del titular de la cuenta.
	 */
	public String getClientDNI(){ return this.clientDNI; }


	/**
	 * Establecer el número IBAN de la cuenta bancaria.
	 * 
	 * @param iban Cadena compuesta por 24 caracteres que almacena el IBAN de la cuenta. P.ej.:
	 * </br></br>
	 * <table>
	 	* <tr>
	 		* <th>CP</th>
	 		* <th></th>
	 		* <th>DC</th>
	 		* <th></th>
	 		* <th>Entidad</th>
	 		* <th></th>
	 		* <th>Oficina</th>
	 		* <th></th>
	 		* <th>DC</th>
	 		* <th></th>
	 		* <th>Nº de cuenta</th>
	 	* </tr>
	 	* <tr>
	 		* <td align="center">ES</td>
	 		* <td></td>
	 		* <td align="center">21</td>
	 		* <td></td>
	 		* <td align="center">1464</td>
	 		* <td></td>
	 		* <td align="center">0100</td>
	 		* <td></td>
	 		* <td align="center">72</td>
	 		* <td></td>
	 		* <td align="center">2030876295</td>
	 	* </tr>
	 * </table>
	 */
	public void setIBAN(String iban){ this.iban = iban; }

	/**
	 * Obtener el número IBAN de la cuenta bancaria.
	 * 
	 * @return Cadena que almacena el número IBAN de la cuenta.
	 */
	public String getIBAN(){ return this.iban; }


	/**
	 * Establecer el balance o saldo de la cuenta.
	 * 
	 * @param balance Valor real para expresar la cantidad de dinero que hay en la cuenta.
	 */
	public void setBalance(double balance){ this.balance = balance; }

	/**
	 * Obtener el saldo de la cuenta.
	 * 
	 * @return Balance monetario de la cuenta, expresado como un número real.
	 */
	public double getBalance(){ return this.balance; }


	//------- Métodos adicionales -------

	//Generar el número IBAN de una cuenta bancaria de forma aleatoria.
	private String generateIBAN(){

		String stControlDigit = this.generateRandomNumber(2),
			   office = this.generateRandomNumber(4),
			   ndControlDigit = this.generateRandomNumber(2),
			   accountNumber = this.generateRandomNumber(10);

		return countryCode + stControlDigit + entity + office + ndControlDigit + accountNumber;
	}


	//Generar un número entero aleatorio de x dígitos.
	private String generateRandomNumber(int length){

		String n = "";

		for(int i=0; i<length; i++)
			n += (int)(Math.random() * 10);

		return n;
	}


	/**
	 * Sobrescritura del método <em>toString()</em> definido en <em>Object</em> para mostrar por consola las
	 * características de la instancia actual.
	 * 
	 * @return Una cadena de caracteres para imprimir en consola un párrafo de texto que enumera el valor
	 * de los atributos de la instancia actual.
	 */
	@Override public String toString(){

		return "IBAN: " + this.iban + "\n" +
			   "Titular: " + Format.CYAN_COLOUR + this.clientDNI + Format.RESET_FORMAT + "\n" +
			   "Saldo: " + this.balance;
	}


	/**
	 * A partir de un vector de Strings, este método construye un objeto <em>Account</em>, interpretando
	 * secuencialmente cada uno de los elementos del vector como un atributo de la instancia.
	 * 
	 * @param accountAttributes Conjunto de atributos que identifican la instancia <em>Account</em> actual,
	 * almacenados como elementos de un vector de cadenas.
	 * 
	 * @return El objeto <em>Account</em> construido a partir del array de Strings, cada uno de cuyos
	 * elementos se convierte en un atributo de la instancia, en el orden en que figuran en el vector
	 * recibido.
	 */
	public static Account toAccount(String[] accountAttributes){

		String iban = accountAttributes[0];
		String dni = accountAttributes[1];
		double balance = Double.parseDouble(accountAttributes[2]);

		return new Account(iban, dni, balance);
	}
}
