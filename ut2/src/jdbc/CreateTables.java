
package jdbc;

import java.sql.*;




/**
 * Esta clase define un único constructor que recibe como parámetro un objeto <em>Connection</em>, el cual almacena
 * una conexión con una base de datos y sobre el que ejecuta dos sentencias prestablecidas para crear las
 * tablas <em>client</em> y <em>account</em>.
 * 
 * <pre>
 * {@code
 * 		Tabla de clientes:
 * 
 *		CREATE TABLE IF NOT EXISTS client (
 *			client_dni CHAR(9),
 *			client_name VARCHAR(34) NOT NULL,
 *			client_surname VARCHAR(34) NOT NULL,
 *			client_birthdate DATE NOT NULL,
 *			client_email VARCHAR(70) NOT NULL,
 *			CONSTRAINT client_pk PRIMARY KEY (client_dni)
 *		)
 * 
 * 
 *		Tabla de cuentas bancarias:
 * 
 *		CREATE TABLE IF NOT EXISTS account ( 
 *			account_iban CHAR(24),
 *			account_client CHAR(9),
 *			account_balance FLOAT,
 *			CONSTRAINT account_pk PRIMARY KEY (account_iban),
 *			CONSTRAINT account_fk_client FOREIGN KEY (account_client) REFERENCES client (client_dni) ON DELETE CASCADE ON UPDATE CASCADE
 *		);
 * }
 * </pre>
 *  
 * @author Juan Elías Risueño Martínez
 */
public class CreateTables{


	//------- Atributos -------

	private Connection connection;
	private Statement statement;
	private static String sql;

	static { sql = ""; }



	//------- Constructores -------

	//Constructor general
	/**
	 * Único constructor de la clase, que lanza las sentencias de creación de las tablas <em>client</em> y
	 * <em>account</em> sobre la base de datos que representa el objeto Connection que recibe como argumento.

	 * @param connection Objeto <em>Connection</em> que mantiene abierta una conexión con la base de datos en que
	 * se crearán las tablas.
	 */
	public CreateTables(Connection connection){

		this.connection = connection;

		try{

			this.statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

			//Creación de la tabla 'client'
			sql = "CREATE TABLE IF NOT EXISTS client (" +
					  "client_dni CHAR(9)," +
					  "client_name VARCHAR(34) NOT NULL," +
					  "client_surname VARCHAR(34) NOT NULL," +
					  "client_birthdate DATE NOT NULL," +
					  "client_email VARCHAR(70) NOT NULL," +
					  "CONSTRAINT client_pk PRIMARY KEY (client_dni)" +
				  ")";

			statement.execute(sql);

			//Creación de la tabla 'account'
			sql = "CREATE TABLE IF NOT EXISTS account (" +
					  "account_iban CHAR(24)," +
					  "account_client CHAR(9)," +
					  "account_balance FLOAT," +
					  "CONSTRAINT account_pk PRIMARY KEY (account_iban)," +
					  "CONSTRAINT account_fk_client FOREIGN KEY (account_client) REFERENCES client (client_dni) ON DELETE CASCADE ON UPDATE CASCADE" +
				  ")";

			statement.execute(sql);
		
		} catch(SQLException sqle){
			sqle.printStackTrace();
		
		} finally{

			try{
				if(statement != null)
					statement.close();

			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}
	}
}
