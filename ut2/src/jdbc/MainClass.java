
package jdbc;

import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.nio.file.FileSystems;
import java.nio.file.Path;




/**
 * Clase ejecutable, en que se instancian todas las demás clases del paquete para demostrar su funcionalidad.
 * 
 * @author Juan Elías Risueño Martínez
 */
public class MainClass{


	//------ Atributos estáticos de clase -------
	private static ProcessBuilder pb;
	private static Process startProcess;

 	static{ pb = new ProcessBuilder("clear"); }

	
	//------- Métodos -------

	/**
	 * Método <em>main()</em>.
	 */
	public static void main(String[] args) throws SQLException{

		Scanner sc = new Scanner(System.in);
		String ip = "", //Definir esta cadena dándole el valor de la dirección IP de un servidor de bases de datos válido.
			   dbName = "bank",
			   user = "postgres",
			   passwd = "bitnami",
			   table,
			   sql,
			   fileName;
		int insertedRows = 0;
		double amount = 7000;
		DataBase db = null;
		Connection connection = null;
		CreateTables createTables = null;
		DataManipulation dataManipulation = null;
		Client c1, c2, c3, c4;
		Account a1, a2, a3;
		ArrayList<Client> clientList = null;
		ArrayList<Account> accountList = null;
		String[][] tableRecords = null;
		String[] pkValues = null;
		Transaction transaction = null;
		Path path = null;
		Procedure procedure = null;
		Function function = null;
		Object[] params = null;
		ResultSet rs = null;



		//------- Descripción de la funcionalidad del programa -------
		System.out.println("\nDESCRIPCIÓN DEL PROGRAMA");
		System.out.println("========================");
		System.out.printf("Este programa ejecutará una serie de operaciones interactuando con un motor de " +
						  "base de datos %sPostgreSQL%s. Durante el proceso, el usuario recibirá información " +
						  "en tiempo real de las tareas realizadas y tendrá la oportunidad de verificar los " +
						  "cambios que estas originan en la base de datos antes de continuar con su ejecución.\n",
						   Format.ITALIC_FONT, Format.RESET_FORMAT);


		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para iniciar la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();



		//------- Creación de la base datos -------
		System.out.println("\n\nCREACIÓN Y CONEXIÓN CON LA DB");
		System.out.println("=============================");

		db = new DataBase(ip, dbName, user, passwd);
		
		if(db.create())
			System.out.printf("%sInfo%s: la base de datos %s%s%s ha sido creada con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, dbName, Format.RESET_FORMAT);
		else{
			System.out.printf("\n%sError%s: no ha sido posible crear la base de datos %s%s%s.\n\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, dbName, Format.RESET_FORMAT);
			System.exit(0);
		}



		//------- Conexión con la base datos -------
		System.out.printf("%sInfo%s: estableciendo conexión con la base de datos %s%s%s...\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, dbName, Format.RESET_FORMAT);
		
		connection = db.getConnection();

		if(connection != null)
			System.out.printf("%sInfo%s: la conexión se ha establecido con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
		else{
			System.out.printf("\n%sError:%s no ha sido posible establecer la conexión.\n\n", Format.RED_COLOUR, Format.RESET_FORMAT);
			System.exit(0);
		}


		
		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();

		

		//------- Creación de tablas -------
		System.out.println("\n\nCREACIÓN DE TABLAS");
		System.out.println("==================");
		
		createTables = new CreateTables(connection);

		if(createTables != null){
			System.out.printf("%sInfo%s: la tabla %sclient%s ha sido creada con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
			System.out.printf("%sInfo%s: la tabla %saccount%s ha sido creada con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
		}
		else{
			System.out.printf("\n%sError%s: no ha sido posible crear las tablas %sclient%s y %saccount%s.\n\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
			System.exit(0);
		}



		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();



		//-------- Inserción de datos -------
		System.out.println("\n\nINSERCIÓN DE DATOS");
		System.out.println("==================");

		//Instanciación de objetos Client y Account, los datos de los cuales se almacenarán en la DB
		
		//Inserción de datos sobre la tabla 'client'
		c1 = new Client("43945124", "Joan", "Margarit", "1938-5-11", "joanmargarit@gmail.com");
		c2 = new Client("52367299", "Robert", "Schuman", "1886-6-29", "robertschuman@yahoo.com");
		c3 = new Client("88456111", "Antonio", "Gaudí", "1852-6-25", "antoniogaudí@outlook.com");
		c4 = new Client("65418118", "Évariste", "Galois", "1811-10-25", "evaristegalois@icloud.com");
		Client[] clients = new Client[]{c1, c2, c3, c4};

		table = "client";
		dataManipulation = new DataManipulation(connection, table);

		for(Client c: clients)
			if(dataManipulation.insertRecord(c))
				insertedRows++;

		System.out.printf("%sInfo%s: se han creado %s%d%s nuevos registros en la tabla %s%s%s.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.CYAN_COLOUR, insertedRows, Format.RESET_FORMAT, Format.ITALIC_FONT, table, Format.RESET_FORMAT);


		//Inserción de datos sobre la tabla 'account'
		a1 = new Account(c1.getDNI(), 97000);
		a2 = new Account(c2.getDNI(), 43000);
		a3 = new Account(c3.getDNI(), 70000);
		Account[] accounts = new Account[]{a1, a2, a3};

		insertedRows = 0;
		table = "account";
		dataManipulation = new DataManipulation(connection, table);

		for(Account a: accounts)
			if(dataManipulation.insertRecord(a))
				insertedRows++;

		System.out.printf("%sInfo%s: se han creado %s%d%s nuevos registros en la tabla %s%s%s.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.CYAN_COLOUR, insertedRows, Format.RESET_FORMAT, Format.ITALIC_FONT, table, Format.RESET_FORMAT);



		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();
		clearConsole();



		//-------- Consulta de datos -------
		System.out.println("\nCONSULTA DE DATOS");
		System.out.println("==================");

		System.out.println("\nCLIENTES");
		System.out.println("--------");

		table = "client";
		dataManipulation = new DataManipulation(connection, table);
		tableRecords = dataManipulation.getRecords();
		clientList = new ArrayList<Client>();

		for(String[] nextRecord: tableRecords)
			clientList.add(Client.toClient(nextRecord));

		clientList.forEach(c -> System.out.println("\n" + c));
		

		System.out.println("\n\nCUENTAS BANCARIAS");
		System.out.println("-----------------");

		table = "account";
		dataManipulation = new DataManipulation(connection, table);
		tableRecords = dataManipulation.getRecords();
		accountList = new ArrayList<Account>();

		for(String[] nextRecord: tableRecords)
			accountList.add(Account.toAccount(nextRecord));

		accountList.forEach(a -> System.out.println("\n" + a));



		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();
		clearConsole();



		//------- Modificación de datos -------
		System.out.println("\nMODIFICACIÓN DE DATOS");
		System.out.println("=====================");

		System.out.println("\nDATOS DE REGISTRO ACTUALES");
		System.out.println("--------------------------");

		table = "client";
		dataManipulation = new DataManipulation(connection, table);
		pkValues = new String[]{c4.getDNI()};
		System.out.println(c4);

		if(dataManipulation.updateRecord("client_email", "galoisevariste@protonmail.com", pkValues))
			System.out.printf("\n\n%sInfo%s: datos de registro actualizados con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
		else
			System.out.printf("\n\n%sError%s: no ha sido posible actualizar los datos de registro.\n", Format.RED_COLOUR, Format.RESET_FORMAT);

		System.out.println("\n\nDATOS DE REGISTRO MODIFICADOS");
		System.out.println("-----------------------------");
		
		tableRecords = dataManipulation.getRecords();
		clientList.clear();

		for(String[] nextRecord: tableRecords)
			clientList.add(Client.toClient(nextRecord));

		clientList.forEach(client -> {

			if(client.getDNI().compareTo(c4.getDNI()) == 0)
				System.out.println(client);
		});



		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();



		//------- Eliminación de datos -------
		System.out.println("\n\nELIMINACIÓN DE DATOS");
		System.out.println("====================");

		if(dataManipulation.deleteRecord(pkValues))
			System.out.printf("%sInfo%s: el cliente %s%s %s%s ha sido eliminado de la base de datos.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.CYAN_COLOUR, c4.getName(), c4.getSurname(), Format.RESET_FORMAT);
		else
			System.out.printf("\n%sError%s: no ha sido posible eliminar el registro.\n", Format.RED_COLOUR, Format.RESET_FORMAT);



		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();
		clearConsole();



		System.out.printf("\n%sInfo%s: en este punto, se cierra la conexión tendida hacia la base de datos %s%s%s en el inicio de la ejecución del programa. Para las operaciones que restan, se hará uso de un %spool%s de conexiones establecido sobre la misma.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, dbName, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
		System.out.print(Format.ITALIC_FONT + "\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();



		System.out.printf("\n\nCREACIÓN DE UN %sPOOL%s DE CONEXIONES\n", Format.ITALIC_FONT, Format.RESET_FORMAT);
		System.out.println("================================");
		System.out.printf("%sInfo%s: cerrando la conexión con la base de datos %s%s%s...\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, dbName, Format.RESET_FORMAT);
		System.out.printf("%sInfo%s: creando %spool%s de conexiones sobre la base de datos %s%s%s...\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT, Format.ITALIC_FONT, dbName, Format.RESET_FORMAT);
		
		if((connection = db.getConnectionPool(5, 10, 100)) != null)
			System.out.printf("%sInfo%s: el %spool%s de conexiones ha sido creado con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
		else{
			System.out.printf("\n%sError%s: no ha sido posible crear el %spool%s de conexiones.\n\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
			System.exit(0);
		}



		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();
		clearConsole();
		


		//------- Transacción -------
		table = "account";

		System.out.println("\n\nEJECUCIÓN DE TRANSACCIONES");
		System.out.println("==========================");

		System.out.printf("\n\n%sInfo%s: esta tarea consiste en la ejecución de una transacción en que se restará " +
						  "la cantidad de %s%.2f%sX a la cuenta de origen para transferirla a la de destino.\n",
						   Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.CYAN_COLOUR, amount, Format.RESET_FORMAT);

		System.out.printf("\n\nDATOS DE REGISTRO ACTUALES - TABLA %s%s%s\n", Format.ITALIC_FONT, table, Format.RESET_FORMAT);
		System.out.println("------------------------------------------");

		dataManipulation = new DataManipulation(connection, table);
		tableRecords = dataManipulation.getRecords();
		accountList.clear();

		for(String[] nextRecord: tableRecords)
			accountList.add(Account.toAccount(nextRecord));
		
		accountList.forEach(a -> {

			if(a.getIBAN().compareTo(a3.getIBAN()) != 0)
				System.out.println("\n" + a);
		});


		/*
		  Definición del objeto Transaction, en que se establece que la cuenta de origen, a la que le será
		  sustraída la cantidad de 'amount', sea a la que hace referencia la instancia Account a1; y que la
		  de destino, a la que le será ingresada la cantidad sustraída, sea a2.
		*/
		amount = 7000;
		transaction = new Transaction(a1, a2, connection, amount);

		//Ejecución de la transacción
		if(transaction.execute())
			System.out.printf("\n\n%sInfo%s: la transacción se ha ejecutado con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
		else
			System.out.printf("\n\n%sError%s: no ha sido posible ejecutar la transacción.\n", Format.RED_COLOUR, Format.RESET_FORMAT);


		System.out.printf("\n\nDATOS DE REGISTRO MODIFICADOS - TABLA %s%s%s\n", Format.ITALIC_FONT, table, Format.RESET_FORMAT);
		System.out.println("---------------------------------------------");

		dataManipulation = new DataManipulation(connection, table);
		tableRecords = dataManipulation.getRecords();
		accountList.clear();

		for(String[] nextRecord: tableRecords)
			accountList.add(Account.toAccount(nextRecord));

		accountList.forEach(a -> {

			if(a.getIBAN().compareTo(a3.getIBAN()) != 0)
				System.out.println("\n" + a);
		});



		System.out.print(Format.ITALIC_FONT + "\n\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();
		clearConsole();



		System.out.println("\n\nCREACIÓN Y EJECUCIÓN DE PROCEDIMIENTOS");
		System.out.println("======================================");

		System.out.printf("\n\n%sInfo%s: el procedimiento que se creará y ejecutará a continuación simula el " +
						  "cobro automático por parte de una entidad bancaria a sus clientes en concepto de " +
						  "gastos de mantenimiento de sus cuentas por valor de %s15.0%sX.\n",
				  		   Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.CYAN_COLOUR, Format.RESET_FORMAT);

		System.out.printf("\n\nDATOS DE REGISTRO ACTUALES - TABLA %s%s%s\n", Format.ITALIC_FONT, table, Format.RESET_FORMAT);
		System.out.println("------------------------------------------");

		accountList.forEach(a -> System.out.println("\n" + a));
		/*
		  Código PostgreSQL para la creación de un procedimiento que que resta 25 unidades monetarias a todas
		  las cuentas de la base de datos en concepto de gastos de mantenimiento.
		*/
		sql = "CREATE OR REPLACE PROCEDURE PUBLIC.charge_maintanance_expenses() LANGUAGE plpgsql " +
		      "AS $procedure$ " +
		      "BEGIN " +
		      "UPDATE account SET account_balance = account_balance - 25; " +
		      "END;" +
		      "$procedure$;";
		/*
		  Para instanciar el objeto Procedure a continuación, se hace uso del constructor que acepta como segundo
		  parámetro de entrada una cadena que define el procedimiento a crear. Para probar el segundo constructor,
		  comentado abajo, y cuyo segundo argumento formal es un objeto Path; comentar la llamada al primer
		  constructor, así como la definición de la varible 'sql' y habilitar las tres líneas de código comentadas
		  a continuación, manteniendo la estructura de directorios tal y como ha sido entregada.
		*/
		procedure = new Procedure(connection, sql);
		/*
		fileName = "./sql/procedure.sql";
		path = FileSystems.getDefault().getPath(fileName);
		procedure = new Procedure(connection, path);
		*/

		//Creación del procedimiento definido en el objeto Procedure.
		if(procedure.create())
			System.out.printf("\n\n%sInfo%s: el procedimiento %scharge_maintanance_expenses()%s ha sido creado con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
		else
			System.out.printf("\n%sError%s: no ha sido posible crear el procedimiento %scharge_maintanance_expenses()%s.\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);

		//Invocación del método que ejecuta el procedimiento.
		if(procedure.call())
			System.out.printf("%sInfo%s: el procedimiento %scharge_maintanance_expenses()%s se ha ejecutado con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
		else
			System.out.printf("\n%sError%s: se ha interrumpido la ejecución del procedimiento %scharge_maintanance_expenses()%s.\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);


		System.out.printf("\n\nDATOS DE REGISTRO MODIFICADOS - TABLA %s%s%s\n", Format.ITALIC_FONT, table, Format.RESET_FORMAT);
		System.out.println("---------------------------------------------");

		tableRecords = dataManipulation.getRecords();
		accountList.clear();

		for(String[] nextRecord: tableRecords)
			accountList.add(Account.toAccount(nextRecord));

		accountList.forEach(a -> System.out.println(a + "\n"));



		System.out.print(Format.ITALIC_FONT + "\nPulse Intro para continuar con la ejecución de tareas..." + Format.RESET_FORMAT);
		sc.nextLine();
		clearConsole();



		System.out.println("\n\nCREACIÓN Y EJECUCIÓN DE FUNCIONES");
		System.out.println("=================================");
		/*
		  Código PostgreSQL para la creación de una función que cuenta la cantidad de clientes de la base de
		  datos con un saldo superior a la cifra recibida como parámetro.
		*/
		sql = "CREATE OR REPLACE FUNCTION PUBLIC.count_clients_richer_than(amount REAL) " +
			  "RETURNS INT " +
			  "LANGUAGE plpgsql " +
			  "AS $function$ " +
			  "DECLARE q INT;" +
			  "BEGIN " +
			  "SELECT COUNT(account_client) INTO q FROM account WHERE account_balance > amount;" +
			  "RETURN q; " +
			  "END;" +
			  "$function$;";
		/*
		  El objeto Function a continuación se ha instanciado a partir del constructor que recibe como segundo
		  parámetro una cadena con la función a crear. Si se quisiese probar el segundo constructor, comentado
		  abajo, y cuyo segundo argumento formal es un objeto Path; basta con comentar la llamada al primer
		  constructor, así como la definición de la varible 'sql' y habilitar las tres líneas de código comentadas
		  a continuación, manteniendo la estructura de directorios tal y como ha sido entregada.
		*/
		function = new Function(connection, sql);
		/*
		fileName = "./sql/function.sql";
		path = FileSystems.getDefault().getPath(fileName);
		function = new Function(connection, path);
		*/

		//Creación de la función definida en el objeto Function.
		if(function.create())
			System.out.printf("%sInfo%s: la función %scount_clients_richer_than(amount REAL)%s para consultar " +
							  "los clientes cuyo saldo sea superior a la cantidad pasada como parámetro ha sido " +
							  "creada con éxito.\n",
							  Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
		else
			System.out.printf("\n%sError%s: no ha sido posible crear la función %scount_clients_richer_than(amount REAL)%s.\n\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);

		//Invocación del método que ejecuta la función.
		amount = 70000;
		params = new Object[]{(float)amount};
		if((rs = function.select(params)) == null)
			System.out.printf("\n%sError%s: se ha interrumpido la ejecución de la función %scount_clients_richer_than(amount REAL)%s.\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);
		else{
			System.out.printf("%sInfo%s: la función %scount_clients_richer_than(amount REAL)%s se ha ejecutado con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, Format.RESET_FORMAT);

			if(!rs.next())
				System.out.printf("\n%Resultado%s: no existen clientes con un saldo superior a %s%.2f%s.\n", Format.UNDERLINE_FONT, Format.RESET_FORMAT, Format.CYAN_COLOUR, params[0], Format.RESET_FORMAT);
			else
				System.out.printf("\n%sResultado%s: existe/n %s%d%s cliente/s con un saldo superior a %s%.2f%s.\n", Format.UNDERLINE_FONT, Format.RESET_FORMAT, Format.CYAN_COLOUR, rs.getInt(1), Format.RESET_FORMAT, Format.CYAN_COLOUR, params[0], Format.RESET_FORMAT);
		}
		



		//------- Cerrar el objeto ResultSet y la conexión con la base de datos -------
		System.out.println("\n\nTÉRMINO DE LAS CONEXIONES");
		System.out.println("=========================");
		
		if(rs != null)
			rs.close();
		
		System.out.printf("%sInfo%s: cerrando la conexión con la base de datos %s%s%s...\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, dbName, Format.RESET_FORMAT);
		
		if(db.disconnect())
			System.out.printf("%sInfo%s: la conexión se ha cerrado con éxito.\n\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
		else
			System.out.printf("\n%sError%s: no ha sido posible cerrar la conexión.\n\n", Format.RED_COLOUR, Format.RESET_FORMAT);
	}



	//Método para limpiar la consola
	private static void clearConsole(){

		try{
			startProcess = pb.inheritIO().start();
			startProcess.waitFor();

		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
