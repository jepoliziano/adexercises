
package jdbc;

import java.sql.*;



/**
 * La clase <em>Transaction</em> ha sido implementada para realizar una transferencia de una cuenta de origen a una cuenta
 * de destino. Dicha transferencia se efectúa en forma de transacción, es decir, si cualquiera de las operaciones
 * de que se compone se viese frustrada, la transeferencia sería anulada en su totalidad, devolviendo la base de
 * datos al estado en que se encontraba antes de su ejecución.
 */
public class Transaction{
	

	//------- Atributos -------

	//Atributos asociados a los objetos de la clase
	private Account sourceAccount;
	private Account targetAccount;
	private Connection connection;
	private double amount;

	//Atributos estáticos
	private static String sql;
	private static PreparedStatement preparedStatement;

	//Sentencia para actualizar el saldo de una cuenta identificada con su clave primaria.
	static{ sql = "UPDATE account SET account_balance = ? WHERE account_iban = ?"; }



	//------- Constructores -------

	/**
	 * Constructor general para definir todos los elementos que intervienen en una transacción.
	 * 
	 * @param sourceAccount Objeto <em>Account</em> que representa la cuenta desde la que se efectuará la transferencia.
	 * @param targetAccount Objeto <em>Account</em> que representa la cuenta receptora.
	 * @param connection Conexión con la base de datos que contiene la tabla en que se listan las cuentas.
	 * @param amount Cantidad a transferir.
	 */
	public Transaction(Account sourceAccount, Account targetAccount, Connection connection, double amount){
		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
		this.connection = connection;
		this.amount = amount;
	}



	//------- Métodos -------

	//Métodos 'setter' y 'getter'
	
	/**
	 * Establecer la cuenta de origen para esta transacción.
	 *
	 * @param sourceAccount Objeto de la clase actual que representa la cuenta de origen de la transacción.
	 */
	public void setSourceAccount(Account sourceAccount){ this.sourceAccount = sourceAccount; }
	
	/**
	 * Obtener la cuenta de origen de esta transacción.
	 *
	 * @return La instancia <em>Account</em> que define la cuenta de origen de la transacción.
	 */
	public Account getSourceAccount(){ return this.sourceAccount; }
	
	
	/**
	 * Establecer la cuenta de destino para esta transacción.
	 *
	 * @param sourceAccount Objeto de la clase actual que representa la cuenta de origen de la transacción.
	 */
	public void setTargetAccount(Account targetAccount){ this.targetAccount = targetAccount; }
	
	/**
	 * Obtener la cuenta de destino de esta transacción.
	 *
	 * @return La instancia <em>Account</em> que define la cuenta de destino de la transacción.
	 */
	public Account getTargetAccount(){ return this.targetAccount; }

	
	/**
	 * Establecer la instancia <em>Connection</em> que almacena la conexión con la base de datos.
	 *
	 * @param Instancia <em>Connection</em> que representa la conexión con la base de datos.
	 */
	public void setConnection(Connection connection){ this.connection = connection; }
	
	/**
	 * Recuperar la conexión  actual con la base de datos.
	 * 
	 * @return Instancia <em>Connection</em> que mantiene abierta una conexión con la base de datos <em>bank</em>.
	 */
	public Connection getConnection(){ return this.connection; }
	
	
	/**
	 * Fijar la suma a traspasar desde la cuenta de origen a la de destino.
	 *
	 * @param Suma a transferir.
	 */
	public void setAmount(double amount){ this.amount = amount; }
	
	/**
 	 * Conseguir la cantidad de dinero transferida en esta transacción.
 	 *
 	 * @return Cantidad transferida de dinero.
	 */
	public double getAmount(){ return this.amount; }


	//Otros métodos

	/**
	 * Ejecuta una transacción a partir de las propiedades de la instancia actual.
	 * 
	 * @return <em>true</em>, si la transacción ha sido ejecutada con éxito; <em>false</em>, en caso contrario.
	 */
	public boolean execute(){

		try{
			//Definición del objeto PreparedStatement
			preparedStatement = this.connection.prepareStatement(sql);

			//Ante todo, hay que modificar el valor por defecto de la propiedad auto-commit.
			this.connection.setAutoCommit(false);

			//Sustracción de la cantidad almacenada en 'balance' de la cuenta de origen.
			preparedStatement.setDouble(1, this.sourceAccount.getBalance() - this.amount);
			preparedStatement.setString(2, this.sourceAccount.getIBAN());
			preparedStatement.executeUpdate();

			//Trasnferencia de la cantidad sustraída a la cuenta de destino.
			preparedStatement.setDouble(1, this.targetAccount.getBalance() + this.amount);
			preparedStatement.setString(2, this.targetAccount.getIBAN());
			preparedStatement.executeUpdate();

			//Ejcución de la transacción
			this.connection.commit();
		
		} catch(SQLException sqle){
			
			sqle.printStackTrace();

			try{
				this.connection.rollback();
				return false;
			
			} catch(SQLException sqle2){
				sqle2.printStackTrace();
			}	
		} finally{

			try{
				this.connection.setAutoCommit(true);
				
				if(preparedStatement != null)
					preparedStatement.close();

			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

		return true;
	}
}
