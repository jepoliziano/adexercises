
package jdbc;

import java.sql.*;
import java.util.ArrayList;




/**
 * A través de esta clase se pueden ejectuar las diferentes operaciones DML para consultar y actualizar el
 * contenido de la base de datos. Se trata, por supuesto, de las sentencias <em>SELECT</em>, <em>INSERT</em>,
 * <em>UPDATE</em> y <em>DELETE</em>.
 * 
 * @author Juan Elías Risueño Martínez
 */
public class DataManipulation {
	

	//------- Atributos -------

	private Connection connection;
	private String table;
	private PreparedStatement preparedStatement;
	private String selectAll;
	/*
	  La consulta que representa la cadena a continuación, devuelve el nombre de la columna que almacena la
	  clave primaria, cuyo valor será almacenado en el array dinámico 'pkColumn', y es que el identificador de una
	  tabla puede estar formado por varios campos.
	*/
	private String selectPK;
	private ArrayList<String> pkColumn;
	/*
	  Cadena que almacena la sentencia SQL que recibirá como parámetro la instancia PreparedStatement
	  en el momento de su construcción.
	*/
	private String preparedStatementStr;
	private Statement statement;
	private ResultSet rs;
	private ResultSetMetaData rsmd; //Para obtener metainformación de las tablas de la base de datos.
	private int columnCount; //Número de columnas de que se compone la tabla representada por el atributo 'table'.
	/*
	  String que estará formado por tantos signos de interrogación como columnas tenga la tabla 'table' y
	  que será integrada al final de la cadena 'preparedStatementStr' para terminar de conformar la sentencia
	  de inserción de datos.
	*/
	private String fieldCount;



	//------- Constructores -------

	/**
	 * Este constructor toma la base de datos con que el objeto <em>Connection</em> especificado en el primer
	 * parámetro mantiene abierta una conexión y prepara los elementos necesarios a través de los cuales poder
	 * realizar operaciones de inserción, modificación, eliminiación y consulta de datos, sobre la tabla indicada
	 * en el segundo argumento.
	 * 
	 * @param connection Objeto que almacena la conexión con la base de datos.
	 * 
	 * @param table Nombre de la tabla de la base de datos representada por la instancia <em>Connection</em> sobre
	 * la que se ejecutarán las sentencias DML.
	 */
	public DataManipulation(Connection connection, String table){

		this.connection = connection;
		this.table = table;
		this.selectAll = "SELECT * FROM " + this.table;

		this.selectPK = "SELECT a.attname AS pkColumn " +
						"FROM pg_index i " +
						"JOIN pg_attribute a ON a.attrelid = i.indrelid " +
						"AND a.attnum = ANY(i.indkey) " +
						"WHERE i.indrelid = '" + this.table + "'::regclass " +
						"AND i.indisprimary " +
						"ORDER BY 1 ASC";

		this.pkColumn = new ArrayList<String>();
		this.preparedStatementStr = "";
		this.statement = null;
		this.rs = null;
		this.rsmd = null;

		try{
			//Definición de la instancia Satatement
			this.statement = this.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

			//ResultSet a partir del cual obtener todos los datos y metadatos de la tabla representada por este objeto.
			this.rs = this.statement.executeQuery(this.selectAll);
			this.rsmd = this.rs.getMetaData();
			this.columnCount = this.rsmd.getColumnCount();
			
			//ResultSet para obtener la clave primaria de la tabla representada por este objeto.
			this.rs = this.statement.executeQuery(this.selectPK);
			
			while(this.rs.next())
				this.pkColumn.add(rs.getString("pkColumn"));

		} catch(SQLException sqle){
			sqle.printStackTrace();
		
		} finally{

			try{
				if(this.rsmd != null){
					this.rs.close();
					this.statement.close();
				}
			
			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}
	}



	//------- Métodos -------
	/*
	  Este método de acceso privado construye un PreparedStatement sobre la conexión a partir de la cual
	  ha sido instanciado el objeto actual para ejecutar sentencias de inserción de datos.
	*/
	private void buildPreparedStatement(){

		this.fieldCount = "";
		this.preparedStatementStr = "INSERT INTO " + this.table + " (";

		try{
			/*
			  Mediante el bucle a continuación se obtiene el nombre de las columnas de que se compone la tabla
			  definida por el segundo argumento del constructor, los cuales serán incoporados a la sentencia de
			  inserción de datos almacenada en el cadena 'preparedStatementStr'. Al mismo tiempo, se construye
			  el String fieldCount en base al recuento de columnas.
			*/
			for(int i=1; i<=this.columnCount; i++){
				this.preparedStatementStr += this.rsmd.getColumnName(i) + ",";
				this.fieldCount += "?,";
			}

			//Composición de la sentencia PreparedStatement.
			this.preparedStatementStr = this.preparedStatementStr.substring(0, preparedStatementStr.length() - 1) + ") VALUES (" + fieldCount.substring(0, fieldCount.length() - 1) + ")";
			
			//Instanciación del PreparedStatement
			this.preparedStatement = this.connection.prepareStatement(this.preparedStatementStr);

		} catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}


	/**
	 * Insertar nuevos registros en la tabla <em>client</em> a partir de la información extraída de los atributos
	 * del objeto <em>Client</em> recibido como parámetro.
	 * 
	 * @param client Instancia <em>Client</em> cuyos atributos definen los datos a introducir en la tabla homónima.
	 * 
	 * @return <em>true</em>, si la sentencia de inserción de datos se ha ejecutado con éxito; <em>false</em>, en
	 * caso contrario.
	 */
	public boolean insertRecord(Client client){

		buildPreparedStatement();

		try{
			this.preparedStatement.setString(1, client.getDNI());
			this.preparedStatement.setString(2, client.getName());
			this.preparedStatement.setString(3, client.getSurname());
			this.preparedStatement.setDate(4, client.getBirthdate());
			this.preparedStatement.setString(5, client.getEmail());
			/*
			  El método executeUpdate() de PreparedStatement devuelve un número entero con el número
			  de filas que se han visto afectadas por la sentencia DML ejecutada o 0 cuando se trata
			  de sentencias SQL que no devuelven nada. Este comportamiento se utiliza aquí para
			  determinar el valor de retorno.
			*/
			if(this.preparedStatement.executeUpdate() == 0)
				return false;

		} catch(SQLException sqle){
			sqle.printStackTrace();
			/*
			  Si saltase la excepción, significaría que la operación de inserción de datos no habrá podido
			  llevarse a cabo.
			*/
			return false;
		} finally{

			try{
				if(this.preparedStatement != null)
					this.preparedStatement.close();

			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}
		/*
		  Alcanzado este punto de la ejecución del método, se puede tener la certeza de que el executeUpdate()
		  ha creado el nuevo registro con éxito.
		*/
		return true;
	}


	/**
	 * Insertar nuevos registros en la tabla <em>account</em> a partir de la información extraída de los atributos
	 * del objeto <em>Account</em> recibido como parámetro.
	 * 
	 * @param account Instancia <em>Account</em> cuyos atributos definen los datos a introducir en la tabla homónima.
	 * 
	 * @return <em>true</em>, si la sentencia de inserción de datos se ha ejecutado con éxito; <em>false</em>, en
	 * caso contrario.
	 */
	public boolean insertRecord(Account account){

		buildPreparedStatement();
		
		try{
			this.preparedStatement.setString(1, account.getIBAN());
			this.preparedStatement.setString(2, account.getClientDNI());
			this.preparedStatement.setDouble(3, account.getBalance());

			if(this.preparedStatement.executeUpdate() == 0)
				return false;

		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		
		} finally{

			try{
				if(this.preparedStatement != null)
					this.preparedStatement.close();
				
			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

		return true;
	}


	/**
	 * Modificar el valor de un campo que almacena una cadena de caracteres para un único registro de la tabla,
	 * identificado por su clave primaria.
	 * 
	 * @param updatedColumn Nombre del campo cuyo valor, de alguno de los tipos CHAR, se desea actualizar.
	 * @param newValue Nuevo dato a almacenar en la tabla.
	 * @param pkValues Valor de la clave primaria del registro a actualizar. En caso de que la clave primaria esté
	 * formada por un conjunto de columnas, indicar el valor de las mismas ordenadas alfabéticamente.
	 * 
	 * @return <em>true</em>, si la sentencia de actualización se ha ejectudo con éxito; <em>false</em>, en
	 * caso contrario.
	 */
	public boolean updateRecord(String updatedColumn, String newValue, String[] pkValues){

		String sql;
		Statement statement = null;

		try{
			statement = this.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			sql = "UPDATE " + this.table + " SET " + updatedColumn + " = '" + newValue + "' WHERE ";

			for(int i=0; i<pkColumn.size(); i++)
				sql += this.pkColumn.get(i) + " = '" + pkValues[i] + "' AND ";

			sql = sql.substring(0, sql.length() - 5);

			statement.executeUpdate(sql);
		
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		
		} finally{

			try{	
				if(statement != null)
					statement.close();
			
			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

		return true;
	}


	/**
	 * Eliminar un registro de la tabla a partir del valor de su clave primaria.
	 * 
	 * @param pkValues Valor de la clave primaria del registro a eliminar. En caso de que la clave primaria esté
	 * formada por un conjunto de columnas, indicar el valor de las mismas ordenadas alfabéticamente.
	 * 
	 * @return <em>true</em>, si la sentencia de elminiación se ha ejectudo con éxito; <em>false</em>, en
	 * caso contrario.
	 */
	public boolean deleteRecord(String[] pkValues){

		Statement statement = null;
		String sql;

		try{
			statement = this.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			sql = "DELETE FROM " + this.table + " WHERE ";
			
			for(int i=0; i<pkColumn.size(); i++)
				sql += this.pkColumn.get(i) + " = '" + pkValues[i] + "' AND ";

			sql = sql.substring(0, sql.length() - 5);

			statement.executeUpdate(sql);

		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		
		} finally{

			try{
				if(statement != null)
					statement.close();
			
			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

		return true;
	}



	/**
	 * Crea una matriz de Strings en que cada fila almacena un registro de la tabla a la que hace referencia la
	 * instancia actual y cada columna un campo de dicha fila.
	 * 
	 * @return Una matriz cada uno de cuyos elementos es un registro de la tabla representada por la instancia
	 * desde la que se invoca.
	 */
	public String[][] getRecords(){

		String[][] tableRecords = null;
		int rowCount;

		try{

			this.statement = this.connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			this.rs = statement.executeQuery(this.selectAll);

			if(!this.rs.next())
				return null;

			this.rs.last();
			rowCount = this.rs.getRow();
			tableRecords = new String[rowCount][this.columnCount];

			this.rs.beforeFirst();

			for(int i=0; this.rs.next(); i++)
				for(int j=0; j<this.columnCount; j++)
					tableRecords[i][j] = String.valueOf(this.rs.getObject(j + 1));

		} catch(SQLException sqle){
			sqle.printStackTrace();
		
		} finally{

			try{
				if(this.rs != null){
					this.rs.close();
					this.statement.close();
				}

			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

		return tableRecords;
	}
}