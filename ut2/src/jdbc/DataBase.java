
package jdbc;

import java.sql.*;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.DbUtils;




/**
 * Define los miembros necesarios para crear una base de datos con codificación de caracteres UTF-8 en un servidor
 * <em>PostgreSQL</em>, así como para establecer conexiones hacia una base de datos existente, satisfaciendo las
 * necesidades de diferentes esquemas de conexión, desde aplicaciones que efectúan una única operación sobre la
 * base de datos hasta programas con múltiples hilos en ejecución, cada uno de los cuales realiza una tarea distinta.
 * 
 * @author Juan Elías Risueño Martínez
 */
public class DataBase{


	//------- Atributos -------

	//Atributos estáticos
	//private static String driver;
	private static String port;
	private static String jdbcurl;
	private static String sql;
	private static Connection connection; 
	private static Statement statement;

	static{
		//driver = "org.postgresql.Driver";
		port = ":5432/";
		jdbcurl = "jdbc:postgresql://";
		sql = "";
		connection = null;
		statement = null;
	}

	//Atributos asociados a la instancia de la clase.
	private String ip; 
	private String dbName;
	private String user;
	private String passwd;



	//------- Constructores -------

	/**
	 * Crea la base de datos <em>dbName</em> en el SGBD <em>PostgreSQL</em> disponible en la dirección de red
	 * <em>ip</em>, proporcionando unas credenciales de usuario (nombre y contraseña) válidas.
	 * 
	 * @param ip Dirección de red en que permanece a la escucha el servidor de bases de datos PostgreSQL.
	 * @param dbName Nombre de la base de datos a crear.
	 * @param user Nombre del usuario con cuenta válida en el servidor.
	 * @param passwd Contraseña de la cuenta.
	 */
	public DataBase(String ip, String dbName, String user, String passwd){

		this.ip = ip;
		this.dbName = dbName;
		this.user = user;
		this.passwd = passwd;
		jdbcurl = jdbcurl + ip + port;		
	}



	//------- Métodos -------
	/*
	//Métodos 'setter' y 'getter'
	public void setIP(String ip){ this.ip = ip; };
	public String getIP(){ return this.ip; }

	public void setDBName(String dbName){ this.dbName = dbName; };
	public String getDBName(){ return this.dbName; }

	public void setUser(String user){ this.user = user; };
	public String getUser(){ return this.user; }

	public void setPasswd(String passwd){ this.passwd = passwd; };
	public String getPasswd(){ return this.passwd; }
	*/


	//Otros métodos

	/**
	 * Con este método se establece una conexión con el motor de <em>PostgreSQL</em> para crear la base de datos
	 * representada por la instancia actual.
	 * <p>
	 * 	NOTA: para cerrar la conexión con el SGBD después de crear la base de datos, este método implementa
	 *	la clase <em>DbUtils</em>, dentro del paquete <em>org.apache.commons.dbutils</em>, la cual facilita
	 *	esta operación, eliminando la necesidad de hacer constar los pertinentes <em>try</em> y <em>catch</em>
	 *	que serían imperativos en caso contrario.
	 * </p>
	 * 
	 * @return <em>true</em>, si la base de datos se ha creado con éxito; <em>false</em>, en caso contrario.
	 */
	public boolean create(){

		try{
			//Cargar el controlador en memoria.
			/*
			  Para la instanciación de esta clase, está previsto que se haga uso de la última versión del controlador de
			  PostgreSQL para el jdbcurl, cuyo 'jar' contiene un fichero interno a partir del cual el controlador se carga
			  en memoria automáticamente con solo indicar su ubicación en el sistema de archivos en el 'class-path'.
			  Versiones anteriores del driver podrían no proporcionar esta facilidad, haciendo necesario cargarlo de forma
			  manual. En tal caso, habilitar la línea de código comentada a continuación, así como el atributo que almacena
			  su nombre como cadena y el punto en el inicializador estático en que se le da valor.
			*/
			//Class.forName(driver).getDeclaredConstructor().newInstance();

			//Conexión con el sistema gestor de base de datos; PostreSQL, para el caso.
			connection = DriverManager.getConnection(jdbcurl, user, passwd);

			//Definición de la instancia Statement con la sentencia de creación de la base de datos.
			statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			
			//Sentencia de eliminiación de la base de datos, si existiere.
			sql = "DROP DATABASE IF EXISTS " + this.dbName;
			statement.execute(sql);

			//Sentencia de creación de la base de datos
			sql = "CREATE DATABASE " + this.dbName + " WITH ENCODING = UTF8";
			statement.execute(sql);
		
		} catch(SQLException sqle){
			//Errores derivados del JDBC
			sqle.printStackTrace();
			return false;
		
		//Habilitar el siguiente 'catch' en caso de cargar el controlador invocando al método Class.forName()
		} /*catch(Exception e){
			//Errores derivados del método Class.forNme()
			e.printStackTrace();
		
		}*/ finally{

			DbUtils.closeQuietly(statement);
			DbUtils.closeQuietly(connection);
		}

		return true;
	}


	/**
	 * Este método establece una conexión con la base de datos que representa la instancia actual y devuelve el
	 * objeto <em>Connection</em> que la almacena.
	 * 
	 * @return El objeto <em>Connection</em> que mantiene abierta una conexión con la base de datos representada
	 * por la instancia de la clase actual.
	 */
	public Connection getConnection(){
		/*
		  Para asegurar que no existe ningún objeto Connection tendido hacia la base de datos, antes de
		  generar una nueva instancia, se cierra la actual, si existiere.
		*/
		if(connection != null)
			disconnect();

		try{
			connection = DriverManager.getConnection(jdbcurl + this.dbName, this.user, this.passwd);
		
		} catch(SQLException sqle){

			sqle.printStackTrace();
			return null;
		}

		return connection;
	}


	/**
	 * Establece un <em>pool</em> de conexiones con la base de datos.
	 * 
	 * @param minIdle Mínimo número de conexiones ociosas.
	 * @param maxIdle Máximo número de conexiones ociosas.
	 * @param maxOpenPreparedStatements Máximo número de sentencias de tipo <em>PreparedStatement</em> abiertas.
	 * 
	 * @return Instancia Connection que almacena el <em>pool</em> de conexiones creado.
	 */
	public Connection getConnectionPool(int minIdle, int maxIdle, int maxOpenPreparedStatements){

		if(connection != null)
			disconnect();

		BasicDataSource bds = new BasicDataSource();

		bds.setUrl(jdbcurl + this.dbName);
		bds.setUsername(this.user);
		bds.setPassword(this.passwd);
		bds.setMinIdle(minIdle);
		bds.setMaxIdle(maxIdle);
		bds.setMaxOpenPreparedStatements(maxOpenPreparedStatements);

		try{
			connection = bds.getConnection();
		
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return null;
		}

		return connection;
	}


	/**
	 * Cierra cualquier conexión abierta con la base de datos representada por el objeto actual.
	 */
	public boolean disconnect(){

		try{
			connection.close();

		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		}

		return true;
	}
}
