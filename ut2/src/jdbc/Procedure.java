
package jdbc;

import java.sql.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.charset.StandardCharsets;
import java.util.List;



/**
 * <em>Procedure</em> contiene los miembros necesarios para realizar tareas básicas en relación con procedimientos
 * en una base de datos, como son la creación y ejecución de los mismos.
 * 
 * @author Juan Elías Risueño Martínez
 */
public class Procedure{


	//------- Atributos -------
	private Connection connection;
	private CallableStatement callableStatement;
	private String creationStatement; //Sentencia SQL para crear el nuevo procedimiento.
	private String callStatement; //Sentencia preparada para ejecutar el procedimiento.



	//------- Constructores -------

	/**
	 * Define un nuevo procedimiento a partir del código en el archivo fuente.
	 * 
	 * @param connection Conexión tendida hacia la base de datos en que se pretende crear el procedimiento.
	 * @param path Instancia de la clase Path que especifica la ruta en el sistema de archivos local en que se ubica
	 * el documento de texto plano que contiene la sentencia a partir de la cual será creado el objeto en la base de
	 * datos.
	 */
	public Procedure(Connection connection, Path path){

		this.connection = connection;
		this.creationStatement = "";
		List<String> script = null;

		try{
			script = Files.readAllLines(path, StandardCharsets.UTF_8);
		
		} catch(IOException ioe){
			ioe.printStackTrace();
		}

		for(String nextLine: script)
			this.creationStatement += nextLine.toLowerCase() + " ";

		findProcedureCallStatement();
	}


	/**
	 * Prepara la sentencia que especifica el parámetro <em>sql</em> para crear un procedimiento en la base
	 * de datos a la que apunta <em>connection</em>.
	 * 
	 * @param connection Conexión tendida hacia la base de datos en que se pretende definir el procedimiento.
	 * @param sql Cadena de caracteres que define la sentencia para crear el procedimiento.
	 */
	public Procedure(Connection connection, String sql){
		this.connection = connection;
		this.creationStatement = sql.toLowerCase();
		findProcedureCallStatement();
	}



	//------- Métodos -------

	/*
	  Método para hallar la sentencia preparada con que definir el objeto PreparedStatement que habrá de lanzar
	  el procedimiento creado sobre la base de datos. De esta forma, en el momento de invocar a this.call() desde
	  la clase ejecutable, la llamada al método solo habrá de recoger los parámetros actuales y no así la sentencia
	  SQL completa.
	*/
	private void findProcedureCallStatement(){

		String next9Chars = null;
		this.callStatement = "";
		int i = 9,
			argumentCount = 0;
		boolean hasArguments = false;

		for(; i<(this.creationStatement.length() - 8); i++)
			if((next9Chars = this.creationStatement.substring((i - 9), i)).compareToIgnoreCase("procedure") == 0)
				break;

		while(this.creationStatement.charAt(i++) != '(')
			this.callStatement += this.creationStatement.charAt(i);

		while(this.creationStatement.charAt(i++) != ')'){

			if(!Character.isSpaceChar(this.creationStatement.charAt(i)))
				hasArguments = true;

			if(this.creationStatement.charAt(i) == ','){
				argumentCount++;
				this.callStatement += "?,";
			}
		}

		this.callStatement += (argumentCount > 0 || hasArguments) ? "?)" : ")";
	}


	/**
	 * Crear el procedimiento definido por la instancia actual.
	 * 
	 * @return <em>true</em>, si la sentencia de creación del objeto se ha ejecutado con éxito; <em>false</em>,
	 * en caso contrario.
	 */
	public boolean create(){

		Statement statement = null;

		try{
			statement = this.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			statement.execute(this.creationStatement);

		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		
		} finally{
			
			try{
				if(statement != null)
					statement.close();

			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

		return true;
	}


	/**
	 * Después de crear el procedimiento con el método <em>create()</em>, este otro lo ejecuta.
	 * 
	 * @return <em>true</em>, si el procedimiento se ha ejecutado correctamente; <em>false</em>, en caso contrario.
	 */
	public boolean call(){

		try{
			this.callableStatement = this.connection.prepareCall("CALL " + this.callStatement);
			this.callableStatement.execute();
		
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		}

		return true;
	}


	/**
	 * Ejecuta el procedimiento parametrizado definido por el objeto actual.
	 * 
	 * @param params Vector de <em>Objects</em> que recoge los parámetros de ejecución del procedimiento.
	 * @return <em>true</em>, si el procedimiento se ha ejecutado correctamente; <em>false</em>, en caso contrario.
	 */
	public boolean call(Object[] params){

		int i = 1;

		try{
			this.callableStatement = this.connection.prepareCall("CALL " + this.callStatement);

			for(Object nextParam: params)
				this.callableStatement.setObject(i++, nextParam);

			this.callableStatement.execute();
		
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		}

		return true;
	}
}