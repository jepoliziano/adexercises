
package jdbc;

import java.sql.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.charset.StandardCharsets;
import java.util.List;



/**
 * De manera paralela a <em>Procedure</em>, <em>Function</em> define los atributos y métodos que hacen falta
 * para crear y ejecutar funciones en una base de datos.
 * 
 * @author Juan Elías Risueño Martínez
 */
public class Function{
	

	//------- Atributos -------
	private Connection connection;
	private CallableStatement callableStatement;
	private String creationStatement; //Sentencia SQL para crear la nueva función.
	private String callStatement; //Sentencia preparada para ejecutar la función.



	//------- Constructores -------

	/**
	 * Dispone los elementos necesarios para crear la función definida en el archivo pasado como parámetro,
	 * invocando, para ello, al método <em>create()</em> de esta misma clase.
	 * 
	 * @param connection Conexión con la base de datos.
	 * @param path Ruta local al <em>script</em> con el código SQL para crear una nueva función.
	 */
	public Function(Connection connection, Path path){

		this.connection = connection;
		this.creationStatement = "";
		List<String> script = null;

		try{
			script = Files.readAllLines(path, StandardCharsets.UTF_8);
		
		} catch(IOException ioe){
			ioe.printStackTrace();
		}

		for(String nextLine: script)
			this.creationStatement += nextLine.toLowerCase() + " ";

		findFunctionSelectStatement();
	}


	/**
	 * En este caso, el código SQL con la sentencia de creación viene dado en forma de cadena de caracteres.
	 * 
	 * @param connection Conexión con la base de datos sobre la que se creará y ejectuará la función.
	 * @param sql Sentencia PostgreSQL para crear una nueva función.
	 */
	public Function(Connection connection, String sql){
		this.connection = connection;
		this.creationStatement = sql.toLowerCase();
		findFunctionSelectStatement();
	}



	//------- Métodos -------

	/*
	  Método para hallar el prototipo de la función a partir del código SQL que la creó. De esta manera,
	  desaparece la necesidad de proporcionar como argumento de entrada del método this.select() la sentencia
	  preparada requerida para ejecutar la función.
	*/
	private void findFunctionSelectStatement(){

		String next8Chars = null;
		this.callStatement = "";
		int i = 8,
			argumentCount = 0;
		boolean hasArguments = false;

		for(; i<(this.creationStatement.length() - 7); i++)
			if((next8Chars = this.creationStatement.substring((i - 8), i)).compareToIgnoreCase("function") == 0)
				break;

		while(this.creationStatement.charAt(i++) != '(')
			this.callStatement += this.creationStatement.charAt(i);

		while(this.creationStatement.charAt(i++) != ')'){

			if(!Character.isSpaceChar(this.creationStatement.charAt(i)))
				hasArguments = true;

			if(this.creationStatement.charAt(i) == ','){
				argumentCount++;
				this.callStatement += "?,";
			}
		}

		this.callStatement += (argumentCount > 0 || hasArguments) ? "?)" : ")";
	}


	/**
	 * Crea en la base de datos un nuevo objeto con la función definida al construir la instancia actual.
	 * 
	 * @return <em>true</em>, si la sentencia de creación lanzada ha tenido éxito; <em>false</em>, en caso contrario.
	 */
	public boolean create(){

		Statement statement = null;

		try{
			statement = this.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			statement.execute(this.creationStatement);

		} catch(SQLException sqle){
			sqle.printStackTrace();
			return false;
		
		} finally{
			
			try{
				if(statement != null)
					statement.close();

			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

		return true;
	}


	/**
	 * Llamada a la función sobre la que se ha construido la instancia actual.
	 * 
	 * @return Objeto <em>ResultSet</em> con el valor devuelto por la función.
	 */
	public ResultSet select(){

		ResultSet rs = null;

		try{
			this.callableStatement = this.connection.prepareCall("SELECT " + this.callStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = this.callableStatement.executeQuery();

		} catch(SQLException sqle){
			sqle.printStackTrace();
			return null;
		}

		return rs;
	}


	/**
	 * Llamada parametrizada a la función sobre la que se ha construido la instancia actual.
	 * 
	 * @param params Lista de parámetros necesarios para ejectuar la función.
	 * @return Colección de resultados devueltos por la ejecución de la función bajo la forma de un <em>ResultSet</em>.
	 */
	public ResultSet select(Object[] params){

		int i = 1;
		ResultSet rs = null;

		try{
			this.callableStatement = this.connection.prepareCall("SELECT " + this.callStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

			for(Object nextParam: params)
				this.callableStatement.setObject(i++, nextParam);

			rs = this.callableStatement.executeQuery();
		
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return null;
		}

		return rs;
	}
}