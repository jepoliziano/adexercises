DESCRIPCIÓN GENERAL
-------------------
	De forma secuencial, este programa opera sobre una base de datos ObjectDB informando dejando constancia de las tareas que se llevan a cabo en cada momento. La ejecución de la aplicación se detendrá entre operación y operación para facilitar su seguimiento.
	El contenido de la base de datos es una adaptación del modelo empleado para la actividad desarrollada con MongoDB. Existen dos clases principales relacionadas entre sí: Composer y Work. Esta última, además, define un atributo complejo de tipo MusicalForm etiquetado como Embedded, el cual, a su vez, incluye un miembro igualmente embebido, que consiste en un tipo Enum para recoger las tres grandes formas musicales: isntrumentales, vocales y mixtas.
	Si bien el programa carece de un mecanismo de entrada de datos, ha sido dotado de ciertas características avanzadas, como una arquitectura MVC y el patrón Singleton. 
