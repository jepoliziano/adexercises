package org.jerm.classicsodb.pojos;

import javax.persistence.*;


@Embeddable
public class MusicalForm implements java.io.Serializable {

    //
    // Atributos
    //
    private String name;
    @Embedded
    private EMusicalFormType musicalFormType;


    //
    // Constructores
    //

    // Constructor por defecto, exigido por JPA
    public MusicalForm(){}

    // Constructor general.
    public MusicalForm(String name, EMusicalFormType musicalFormType) {
        this.name = name;
        this.musicalFormType = musicalFormType;
    }


    //
    // Métodos 'getter' y 'setter'
    //
    public String getName(){ return this.name; }
    public void setName(){ this.name = name; }

    public EMusicalFormType getMusicalFormType(){ return this.musicalFormType; }
    public void setMusicalFormType(EMusicalFormType musicalFormType){ this.musicalFormType = musicalFormType; }
}
