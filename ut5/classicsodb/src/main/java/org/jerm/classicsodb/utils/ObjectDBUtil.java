package org.jerm.classicsodb.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


// Implementación del patron 'creacional' Singleton para obtener una única
// instancia EntityManager que reutilizar a lo largo de toda la aplicación.
public class ObjectDBUtil {

    // Atributo para almacenar la instancia EntityManager.
    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;
    private static String url;

    // Inicializador estático para instanciar el objeto EntityManager
    // la primera vez que se ejecute la clase ObjectDBUtil.
    static {
        url = "objectdb:db/classics.odb";
        entityManagerFactory = Persistence.createEntityManagerFactory(url);
        entityManager = entityManagerFactory.createEntityManager();
    }

    // Constructor privado, según el patrón Singleton
    private ObjectDBUtil(){}

    // Método para devolver la instancia EntityManager creada.
    public static EntityManager getEntityManager(){
        return entityManager;
    }
}