package org.jerm.classicsodb.crud;


import javax.persistence.EntityTransaction;

import org.jerm.classicsodb.pojos.Composer;
import org.jerm.classicsodb.pojos.EMusicalFormType;
import org.jerm.classicsodb.pojos.MusicalForm;
import org.jerm.classicsodb.pojos.Work;

import static org.jerm.classicsodb.utils.ObjectDBUtil.getEntityManager;

import java.util.Arrays;


public class Create {

    // Agregar un nuevo compositor a la base de datos, como consecuencia de lo cual, se
    // registrarán también las obras del mismo y la forma musical de cada una de ellas.
    public static int createComposer(){

        //Número total de objetos Composer de la base de datos, antes de ejecutar la inserción.
        int totalComposers = Retrieve.retrieveComposerList().size();

        MusicalForm symphony = new MusicalForm("Sinfonía", EMusicalFormType.INSTRUMENTAL_FORM);
        MusicalForm quartet = new MusicalForm("Cuarteto", EMusicalFormType.INSTRUMENTAL_FORM);
        MusicalForm overture = new MusicalForm("Obertura", EMusicalFormType.INSTRUMENTAL_FORM);

        EntityTransaction entityTransaction = getEntityManager().getTransaction();
        entityTransaction.begin();

        Composer composer = new Composer("Gustav Mahler", "G. Mahler");
        Work work1 = new Work("Sinfonía n.º 5 en do sostenido menor", symphony, composer);
        Work work2 = new Work("Cuarteto para piano en la menor", quartet, composer);
        composer.addWorks(Arrays.asList(work1, work2));
        getEntityManager().persist(composer);

        composer = new Composer("Piotr Ilich Tchaikovski", "Tchaikovski");
        work1 = new Work("Sinfonía n.º 5 en mi menor, Op. 64", symphony, composer);
        work2 = new Work("Obertura 1812, Op. 49", overture, composer);
        composer.setWorks(Arrays.asList(work1, work2));
        getEntityManager().persist(composer);

        entityTransaction.commit();

        return Retrieve.retrieveComposerList().size() - totalComposers;
    }
}
