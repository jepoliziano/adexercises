package org.jerm.classicsodb.views;

import org.jerm.classicsodb.pojos.Composer;
import org.jerm.classicsodb.utils.Format;

import java.util.List;


// Clase que define el conjunto de métodos empleados para volcar información en consola.
public class ConsoleView {


    // Mensaje inicial, con que se informa al usuario de la funcionalidad de la aplicación.
    public static void showInitialMessage(){

        System.out.println("DESCRIPCIÓN DE USO");
        System.out.println("~~~~~~~~~~~~~~~~~~");

        System.out.printf(
                "De forma secuencial, este programa opera sobre una base de datos %sObjectDB%s, " +
                "informando al usuario de las tareas que se llevan a cabo.\nLa ejecución de " +
                "la aplicación se detendrá entre operación y operación para facilitar el seguimiento " +
                "de las mismas.\n",
                Format.ITALIC_FONT,
                Format.RESET_FORMAT
        );
    }


    // Mostrar mensaje de creación de nuevo objeto.
    public static void showNewObjectMessage(int count){

        System.out.printf(
                "\n%sInfo%s: se han creado %s%d%s nuevos objetos %sComposer%s.\n",
                Format.GREEN_COLOUR,
                Format.RESET_FORMAT,
                Format.ITALIC_FONT,
                count,
                Format.RESET_FORMAT,
                Format.ITALIC_FONT,
                Format.RESET_FORMAT
        );
    }


    // Mostrar mensaje de modificación de objeto.
    public static void showUpdatedObjectMessage(int composerId){
        System.out.printf(
                "\n%sInfo%s: se han actualizado los datos del objeto %sComposer%s con ID %s%d%s.\n",
                Format.GREEN_COLOUR,
                Format.RESET_FORMAT,
                Format.ITALIC_FONT,
                Format.RESET_FORMAT,
                Format.CYAN_COLOUR,
                composerId,
                Format.RESET_FORMAT
        );
    }


    // Mostrar mensaje de eliminación de objeto.
    public static void showRemovedObjectMessage(int composerId){
        System.out.printf(
                "\n%sInfo%s: se ha eliminado el objeto %sComposer%s con ID %s%d%s.\n",
                Format.GREEN_COLOUR,
                Format.RESET_FORMAT,
                Format.ITALIC_FONT,
                Format.RESET_FORMAT,
                Format.CYAN_COLOUR,
                composerId,
                Format.RESET_FORMAT
        );
    }


    // Mostrar los datos de un compositor.
    public static void printComposerData(List<Composer> composers){

        System.out.println("\nBBDD CLÁSICOS POPULARES - OBJETOS");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        composers.forEach(composer -> {

            System.out.println("\nCOMPOSITOR");
            System.out.println("==========");

            System.out.println("ID: " + composer.getId());
            System.out.println("Nombre: " + composer.getName());
            System.out.printf("Sobrenombre: %s%s%s\n", Format.CYAN_COLOUR, composer.getAlias(), Format.RESET_FORMAT);

            System.out.println("\nOBRAS");
            System.out.println("-----");

            composer.getWorks().forEach(work -> {
                System.out.println("ID: " + work.getId());
                System.out.printf("Título: %s%s%s\n", Format.CYAN_COLOUR, work.getTitle(), Format.RESET_FORMAT);
                System.out.println("Forma musical: " + work.getMusicalForm().getName());
            });
        });
    }
}
