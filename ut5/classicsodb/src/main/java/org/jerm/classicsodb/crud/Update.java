package org.jerm.classicsodb.crud;


import org.jerm.classicsodb.pojos.Composer;

import javax.persistence.EntityTransaction;
import static org.jerm.classicsodb.utils.ObjectDBUtil.getEntityManager;

public class Update {

    // Modificar los datos de registro de uno de los objetos Composer almacenados.
    public static int updateComposer(){

        String filterAlias = "Tchaikovski",
               newName = "Piotr Ilich Chaikovsky",
               newAlias = "Chaikovsky";
        Composer composer = Retrieve.retrieveSingleComposer(filterAlias);
        EntityTransaction entityTransaction = getEntityManager().getTransaction();
        entityTransaction.begin();
        composer.setName(newName);
        composer.setAlias(newAlias);
        entityTransaction.commit();

        return composer.getId();
    }
}
