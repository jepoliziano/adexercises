package org.jerm.classicsodb.pojos;

import javax.persistence.*;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.List;


@Entity
public class Composer implements Serializable{

    //
    // Atributos
    //
    @Id
    @GeneratedValue
    private int id;

    private String name;

    private String alias;

    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Work> works;


    //
    // Constructores
    //

    // Constructor por defecto, exigido por JPA.
    public Composer(){
        this.works = new ArrayList<>();
    }

    // Constructor general, para especificar el nombre y el sobrenombre de un compositor.
    public Composer(String name, String alias){
        this.name = name;
        this.alias = alias;
        this.works = new ArrayList<>();
    }

    // Constructor general para definir todos atributos de instancia
    public Composer(String name, String alias, List<Work> works){
        this.name = name;
        this.alias = alias;
        this.works = works;
    }


    //
    // Métodos 'getter' y 'setter'
    //

    public int getId() { return id; }

    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }

    public String getAlias() { return alias; }
    public void setAlias(String alias) { this.alias = alias; }

    public List<Work> getWorks() { return works; }
    public void setWorks(List<Work> works) { this.works = works; }


    //
    // Métodos adicionales
    //

    // Añadir una nueva instancia Work a
    // la lista de obras de este objeto.
    public void addWork(Work work){
        this.works.add(work);
    }

    // Añadir una lista de obras a la instancia actual
    public void addWorks(List<Work> works){
        this.works.addAll(works);
    }
}
