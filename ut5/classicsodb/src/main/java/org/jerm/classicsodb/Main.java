package org.jerm.classicsodb;

import org.jerm.classicsodb.utils.ObjectDBUtil;
import static org.jerm.classicsodb.Controller.runApplication;


public class Main {
    public static void main(String[] args){

        // Punto de entrada de la aplicación
        runApplication();
        ObjectDBUtil.getEntityManager().close();
    }
}
