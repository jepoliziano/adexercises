
package org.jerm.classicsodb.utils;


/**
 * Contiene una serie de constantes para dar formato a la salida del texto en consola a partir de secuencias de escpae Unicode.
 *
 * @author Juan Elías Risueño Martínez
 */
public class Format {

    /**
     * Mostrar la fuente en cursiva.
     */
    public final static String ITALIC_FONT;
    /**
     * Subrayar texto.
     */
    public final static String UNDERLINE_FONT;
    /**
     * Seleccionar rojo como color de fuente.
     */
    public final static String RED_COLOUR;
    /**
     * Seleccionar verde como color de fuente.
     */
    public final static String GREEN_COLOUR;
    /**
     * Seleccionar azul cyan como color de fuente.
     */
    public final static String CYAN_COLOUR;
    /**
     * Devolver la fuente su formato por defecto.
     */
    public final static String RESET_FORMAT;

    static{
        ITALIC_FONT = "\033[3m";
        UNDERLINE_FONT = "\033[4m";
        RED_COLOUR = "\033[31m";
        GREEN_COLOUR = "\033[32m";
        CYAN_COLOUR = "\033[36m";
        RESET_FORMAT = "\u001B[0m";
    }
}
