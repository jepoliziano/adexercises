package org.jerm.classicsodb.pojos;

import javax.persistence.*;


@Embeddable
public enum EMusicalFormType implements java.io.Serializable{

    //
    // Constantes del enumerado
    //
    INSTRUMENTAL_FORM("Forma instrumental"),
    VOCAL_FORM("Forma vocal"),
    MIX_FORM("Forma mixta");


    //
    // Atributos
    //
    private String name;


    //
    // Constructores
    //

    // Constructor por defecto, exigido por JPA.
    // NOTA: para poder crear un enumerado con atributos, el modificador de acceso de los
    // constructores no puede ser público. Para el caso, se queda el que se asigna por defecto.
    EMusicalFormType(){}

    // Constructor general.
    EMusicalFormType(String name){
        this.name = name;
    }


    //
    // Métodos 'getter' y 'setter'
    //
    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }
}
