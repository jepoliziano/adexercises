package org.jerm.classicsodb;

import org.jerm.classicsodb.crud.Create;
import org.jerm.classicsodb.crud.Delete;
import org.jerm.classicsodb.crud.Retrieve;
import org.jerm.classicsodb.crud.Update;
import org.jerm.classicsodb.utils.Format;
import org.jerm.classicsodb.views.ConsoleView;

import java.util.Scanner;


public class Controller {

    //
    // Atributos
    //
    private static final Scanner input;


    //
    // Inicializador estático
    //
    static{
        input = new Scanner(System.in);
    }


    //
    // Métodos
    //

    // Método invocado con la ejecución del programa.
    public static void runApplication(){

        // Mostrar el mensaje con la descripción de la funcionalidad.
        ConsoleView.showInitialMessage();
        pauseExecution();

        // Inserción de datos.
        ConsoleView.showNewObjectMessage(Create.createComposer());
        pauseExecution();

        // Mostrar el contenido de la base datos, después de la inserción.
        ConsoleView.printComposerData(Retrieve.retrieveComposerList());
        pauseExecution();

        // Modificación de datos.
        ConsoleView.showUpdatedObjectMessage(Update.updateComposer());
        pauseExecution();

        // Mostrar el contenido de la base datos, después de la modificación.
        ConsoleView.printComposerData(Retrieve.retrieveComposerList());
        pauseExecution();

        // Eliminación de datos.
        ConsoleView.showRemovedObjectMessage(Delete.deleteComposer());
        pauseExecution();

        // Mostrar el contenido de la base datos, después de la eliminación.
        ConsoleView.printComposerData(Retrieve.retrieveComposerList());
    }


    //Detener la ejecución del programa hasta que el usuario pulse una tecla.
    private static void pauseExecution(){

        System.out.printf(
                "\n%sPulse INTRO para continuar con la ejecución del programa...%s",
                Format.ITALIC_FONT,
                Format.RESET_FORMAT
        );

        input.nextLine();
    }
}




























