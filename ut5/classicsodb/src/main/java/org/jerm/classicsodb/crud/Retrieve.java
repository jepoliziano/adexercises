package org.jerm.classicsodb.crud;

import org.jerm.classicsodb.pojos.Composer;
import static org.jerm.classicsodb.utils.ObjectDBUtil.getEntityManager;

import javax.persistence.TypedQuery;
import java.util.List;


public class Retrieve {

    // Recuperar el conjunto de objetos Composer
    // de la base de datos en forma de lista.
    public static List<Composer> retrieveComposerList(){

        String selectAll = "SELECT c FROM Composer AS c";
        TypedQuery<Composer> typedQuery = getEntityManager().createQuery(selectAll, Composer.class);
        return typedQuery.getResultList();
    }

    // Leer un compositor, a partir de su sobrenombre.
    public static Composer retrieveSingleComposer(String filterAlias){

        String paramSelect = "SELECT c FROM Composer AS c WHERE c.alias = :alias";
        TypedQuery<Composer> typedQuery = getEntityManager().createQuery(paramSelect, Composer.class);
        typedQuery.setParameter("alias", filterAlias);

        return typedQuery.getSingleResult();
    }
}
