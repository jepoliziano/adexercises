package org.jerm.classicsodb.crud;

import org.jerm.classicsodb.pojos.Composer;
import javax.persistence.EntityTransaction;
import static org.jerm.classicsodb.utils.ObjectDBUtil.getEntityManager;


public class Delete {

    // Eliminar un objeto de la base de datos
    public static int deleteComposer(){

        String filterAlias = "G. Mahler";
        Composer composer = Retrieve.retrieveSingleComposer(filterAlias);
        EntityTransaction entityTransaction = getEntityManager().getTransaction();
        entityTransaction.begin();
        getEntityManager().remove(composer);
        entityTransaction.commit();

        return composer.getId();
    }
}
