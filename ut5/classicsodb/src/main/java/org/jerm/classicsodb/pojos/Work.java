package org.jerm.classicsodb.pojos;

import javax.persistence.*;
import java.io.Serializable;


@Entity
public class Work implements Serializable {

    //
    // Atributos
    //
    @Id
    @GeneratedValue
    private int id;

    private String title;

    @Embedded
    private MusicalForm musicalForm;

    @ManyToOne
    private Composer composer;


    //
    // Constructores
    //

    // Constructor por defecto, exigido por JPA.
    public Work(){}

    // Constructor general
    public Work(String title, MusicalForm musicalForm, Composer composer){
        this.title = title;
        this.musicalForm = musicalForm;
        this.composer = composer;
    }


    //
    // Métodos 'getter' y 'setter'
    //

    public int getId() { return id; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public MusicalForm getMusicalForm() { return musicalForm; }
    public void setMusicalForm(MusicalForm musicalForm) { this.musicalForm = musicalForm; }

    public Composer getComposer(){ return this.composer; }
    public void setComposer(Composer composer){ this.composer = composer; }
}
