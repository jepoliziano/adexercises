
package com.jerm.ecommerce;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;


public class CustomerModel {

    //------- Atributos -------

    private Session session;

    private static Transaction transaction;
    private static Query query;
    private static String hql;
    private static Iterator iterator;
    private static Customer customer;

    //------- Constructores -------

    //Constructor por defecto
    public CustomerModel(Session session){
        this.session = session;
    }


    //------- Métodos 'setter' y 'getter' -------

    public void setSession(Session session){ this.session = session; }
    public Session getSession() { return this.session; }


    //------- Métodos de instancia -------

    //Agregar un nuevo registro a la tabla de clientes.
    public boolean insertCustomer(Customer newCustomer){

        List<Product> products = new Vector<>();
        Product product;

        //Solo si el email del cliente a registrar no coincide con ninguno
        //de los de la base de datos se ejecuta la operación de inserción.
        if(searchCustomer(newCustomer.getEmail(), this.session) == null){
            //Ahora, para cada uno de los productos asociados desde el formulario de inserción
            //con esta instancia Customer, se comprueba que su nombre no coincida con ninguno
            //de los ya registrados. Si así fuese, el producto en cuestión sería sustituido por
            //el que ya figura en la base de datos con el mismo nombre, evitando duplicidades,
            //pero permitiendo que un mismo producto pueda estar vinculado a clientes distintos.
            //Además, si se introdujese un nuevo cliente asociándolo a productos ya existentes,
            //pero se modificase el precio de los mismos, los cambios se verían reflejados.
            for(Product nextProduct: newCustomer.getProducts()){

                product = ProductModel.searchProduct(nextProduct.getName(), this.session);

                if(product != null){
                    product.setPrice(nextProduct.getPrice());
                    products.add(product);
                } else
                    products.add(nextProduct);
            }

            newCustomer.setProducts(products);
            transaction = this.session.beginTransaction();
            this.session.save(newCustomer);
            transaction.commit();
            return true;
        }

        return false;
    }


    //Modificar los datos de un cliente.
    public boolean updateCustomer(Customer replacementCustomer){

        Customer updatedCustomer = searchCustomer(replacementCustomer.getEmail(), this.session);
        Product product;
        List<Product> products = new Vector<>();

        //La siguiente condición se cumple si no hay ningún usuario cuyo email coincida
        //con los datos de actualización o coincide, pero se trata del propio cliente.
        if(null == updatedCustomer || updatedCustomer.getCode() == replacementCustomer.getCode()){

            for(Product nextProduct: replacementCustomer.getProducts()){

                product = ProductModel.searchProduct(nextProduct.getName(), this.session);

                if(product != null){
                    product.setPrice(nextProduct.getPrice());
                    products.add(product);
                } else
                    products.add(nextProduct);
            }

            transaction = this.session.beginTransaction();
            updatedCustomer = session.load(Customer.class, replacementCustomer.getCode());
            updatedCustomer.setEmail(replacementCustomer.getEmail());
            updatedCustomer.setUsername(replacementCustomer.getUsername());
            updatedCustomer.setProducts(products);
            transaction.commit();
            return true;
        }

        return false;
    }


    //Eliminar un cliente.
    public boolean deleteCustomer(Customer deletedCustomer){

        transaction = this.session.beginTransaction();
        hql = "DELETE FROM Customer WHERE code = :code";
        query = this.session.createQuery(hql);
        query.setParameter("code", deletedCustomer.getCode());

        if(query.executeUpdate() == 1) {
            transaction.commit();
            return true;
        }

        return false;
    }


    //Consultar la lista de productos de un cliente
    public Iterator<Product> selectCustomerProducts(Customer customer){

        hql = "SELECT c.products FROM Customer c WHERE c.code = :code";
        query = this.session.createQuery(hql);
        query.setParameter("code", customer.getCode());
        return query.iterate();
    }


    //------- Métodos de estáticos -------

    //Buscar un cliente en la BBDD para la sesión indicada.
    public static Customer searchCustomer(String email, Session session){

        hql = "FROM Customer WHERE email = :email";
        query = session.createQuery(hql);
        query.setParameter("email", email);
        return (Customer)query.uniqueResult();
    }
}