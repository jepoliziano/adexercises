
package com.jerm.ecommerce;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


public class ProductModel {

    //------- Atributos -------

    private Session session;

    private static Transaction transaction;
    private static Query query;
    private static String hql;
    private static Iterator iterator;
    private static Product product;


    //------- Constructores -------

    //Constructor por defecto
    public ProductModel(Session session) {
        this.session = session;
    }


    //------- Métodos 'setter' y 'getter' -------

    public void setSession(Session session){ this.session = session; }
    public Session getSession() { return this.session; }


    //------- Métodos de instancia -------

    //Agregar un nuevo registro a la tabla de productos o modificarlo.
    //Puesto que la entidad Product no es la propietaria de la relación Customer <-> Product, para que, al
    //crear un nuevo producto en la BBDD, la tabla intermedia actualice su información, el objeto que debe
    //ser incorporado al contexto de persistencia no puede ser el producto, sino el cliente. La acción a
    //ejecutar tanto para crear como para modificar un producto, por tanto, es idéntica, pues se trata de
    //hacer persistir los clientes asociados a un producto que no figuren ya en la BBDD y sus productos asociados.
    public boolean insertOrUpdateProduct(Product product) {

        product.getCustomers().forEach(customer -> {
            if (CustomerModel.searchCustomer(customer.getEmail(), this.session) == null)
                this.session.save(customer);
        });

        transaction = this.session.beginTransaction();
        transaction.commit();

        return transaction.getStatus() == TransactionStatus.COMMITTED;
    }


    //Eliminar un producto.
    public boolean deleteProduct(Product deletedProduct){

        transaction = this.session.beginTransaction();
        hql = "DELETE FROM Product WHERE code = :code";
        query = this.session.createQuery(hql);
        query.setParameter("code", deletedProduct.getCode());

        if(query.executeUpdate() == 1) {
            transaction.commit();
            return true;
        }

        return false;
    }


    //Consultar la lista de productos de un cliente
    public Iterator<Customer> selectCustomerProducts(Product product){

        hql = "SELECT p.customers FROM Product p WHERE p.code = :code";
        query = this.session.createQuery(hql);
        query.setParameter("code", product.getCode());
        return query.iterate();
    }


    //------- Métodos estáticos -------

    //Buscar un producto en la BBDD dentro de la sesión indicada.
    public static Product searchProduct(String productName, Session session) {

        hql = "FROM Product WHERE name = :name";
        query = session.createQuery(hql);
        query.setParameter("name", productName);
        return (Product)query.uniqueResult();
    }
}