
package com.jerm.ecommerce;

import javax.persistence.*;
import java.util.List;
import java.util.Vector;


/**
 * POJO que representa los clientes de la base de datos concebida para este paquete de clases. Los objetos
 * <em>Customer</em> están identificados, ademas de por la clave primaria, con una dirección de correo
 * electrónico y un nombre usuario.
 */
@Entity
@Table(name="client", schema="public", catalog="ecommerce")
public class Customer implements java.io.Serializable{

    //------- Atributos -------

    @Id
    @Column(name="customer_id")
    @GeneratedValue(generator="customerIdGenerator")
    @SequenceGenerator(name="customerIdGenerator", sequenceName="seq_customer_id", allocationSize=1)
    private int code;

    @Column(name="customer_email")
    private String email;

    @Column(name="customer_username")
    private String username;

    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(
            name="customer_product",
            joinColumns = @JoinColumn(name="customer_id", referencedColumnName="customer_id"),
            inverseJoinColumns = @JoinColumn(name="product_id",  referencedColumnName="product_id")
    )
    private List<Product> products;


    //------- Constructores -------

    //Constructor por defecto vacío, según las directrices de JPA.
    public Customer(){
        this.products = new Vector<>();
    }

    /**
     * Constructor general para definir el valor de los atributos con que se identifica al usuario del
     * comercio electrónico, teniendo en cuenta el mecanismo de autogeneracion de claves primarias.
     *
     * @param email Correo electrónico del cliente.
     * @param username Nombre de usuario del cliente.
     */
    public Customer(String email, String username, List<Product> products){
        this.email = email;
        this.username = username;
        this.products = products;
    }


    //------- Métodos 'setter' y 'getter' -------

    /**
     * Obtener el identificador autogenerado del cliente.
     *
     * @return Entero con que el cliente se identifica en la base de datos.
     */
    public Integer getCode(){ return this.code; }

    /**
     * Establecer el correo electrónico del cliente.
     *
     * @param email Cadena de caracteres con el correo electrónico del cliente.
     */
    public void setEmail(String email){ this.email = email; }
    /**
     * Obtener el correo electrónico del cliente.
     * @return Cadena de caracteres que almacena el correo electrónico del cliente.
     */
    public String getEmail(){ return this.email; }

    /**
     * Definir el nombre de usuario del cliente.
     *
     * @param username String con el nombre de usuario del cliente.
     */
    public void setUsername(String username){ this.username = username; }

    /**
     * Obtener el nombre del cliente.
     *
     * @return String con el nombre de usuario del cliente.
     */
    public String getUsername(){ return this.username; }

    /**
     * Establecer la lista de productos asociada a un usuario.
     *
     * @param products Colección de objetos Product adquiridos por el usuario.
     */
    public void setProducts(List<Product> products){ this.products = products; }

    /**
     * Obtener la lista de productos asociada a un usuario.
     *
     * @return Listado de productos vinculado con este cliente.
     */
    public List<Product> getProducts(){ return this.products; }


    //------- Métodos adicionales -------

    /**
     * Sobrescritura del método toString() definido en Object para imprimir por consola las características
     * de la instancia actual.
     *
     * @return Una cadena de caracteres para imprimir en consola un párrafo de texto que muestra el valor
     * de los atributos del cliente actual.
     */
    @Override public String toString(){

        return "Id: " + Format.CYAN_COLOUR + this.code + Format.RESET_FORMAT + "\n" +
               "E-mail: " + this.email + "\n" +
               "Nombre de usuario: " + this.username ;
               //"Productos adquiridos: " + this.products;
    }
}
