package com.jerm.ecommerce;

public class View {

    //------- Métodos para imprimir en consola las diferentes vistas de la aplicación -------

    //Menú principal.
    public void showMainMenu(){

        System.out.println(Format.RESET_FORMAT);
        System.out.println("   ******************************");
        System.out.println("   *           MENÚ             *");
        System.out.println("   ******************************");
        System.out.println("   *                            *");
        System.out.println("   *  1) Nuevo registro.        *");
        System.out.println("   *  2) Modificar registro.    *");
        System.out.println("   *  3) Eliminar registro.     *");
        System.out.println("   *  4) Consultar registro.    *");
        System.out.println("   *  0) Salir.                 *");
        System.out.println("   *                            *");
        System.out.println("   ******************************");
    }


    //Menú de selección de tabla
    public void showTablesMenu(){

        System.out.println(Format.RESET_FORMAT);
        System.out.println("   *********************");
        System.out.println("   * TABLAS DE LA BBDD *");
        System.out.println("   *********************");
        System.out.println("   *                   *");
        System.out.println("   *   1) Cliente.     *");
        System.out.println("   *   2) Producto.    *");
        System.out.println("   *   0) Salir.       *");
        System.out.println("   *                   *");
        System.out.println("   *********************");
    }


    //Mostrar un mensaje que pregunta al usuario si desea abandonar la aplicación.
    public void showStayMessage() {
        System.out.print(Format.RESET_FORMAT + "\n¿Desea realizar alguna otra operación? [s,n] " + Format.CYAN_COLOUR);
    }


    //Mostrar la cadena recibida por parámetro como un título.
    public void showTitle(String title){

        int titleLength = title.length();

        System.out.println(Format.RESET_FORMAT);
        System.out.println(title.toUpperCase());

        for(int i=titleLength; i>0; i--)
            System.out.print(1 == i ? "\n" : "~");
    }


    //Mostrar la información de un cliente de la base de datos, junto con sus productos asociados.
    public void showEntityData(Customer customer){

        this.showTitle("\ndatos del cliente");
        System.out.println(customer);

        this.showTitle("lista de productos asociados ");
        customer.getProducts().forEach(product -> System.out.println(product + "\n"));
    }


    //Mostrar la información de un producto de la base de datos, junto con sus clientes asociados.
    public void showEntityData(Product product){

        this.showTitle("\ndatos del producto");
        System.out.println(product);

        this.showTitle("lista de clientes asociados ");
        product.getCustomers().forEach(customer -> System.out.println(customer + "\n"));
    }


    //Mostrar un mensaje solicitando la inserción de datos.
    public void showInsertDataMessage(String textToShow){
        System.out.print(Format.RESET_FORMAT);
        System.out.print(textToShow + ": " + Format.CYAN_COLOUR);
        System.out.print(Format.CYAN_COLOUR);
    }


    //Mostrar un mensaje ante el cual el usuario debe responder 'sí' o 'no'.
    public void showYesOrNoMessage(String textToShow){
        System.out.print(Format.RESET_FORMAT);
        System.out.print("\n¿" + textToShow + "? [s,n] ");
        System.out.print(Format.CYAN_COLOUR);
    }


    //Mostrar un mensaje que pide al usuario que seleccione una de las opciones disponibles.
    public void showChooseOptionMessage(int totalOptions, boolean includeZero){

        String chooseOptionMessage = Format.RESET_FORMAT +"\nSeleccione una opción [";

        for(int i=1; i<=totalOptions; i++)
            chooseOptionMessage += (i + ",");

        chooseOptionMessage = (includeZero)
                              ? chooseOptionMessage + "0] "
                              : chooseOptionMessage.substring(0,chooseOptionMessage.length() - 1) + "] ";

        System.out.print(chooseOptionMessage + Format.CYAN_COLOUR);
    }


    //Nuevo registro creado con éxito.
    public void showInsertionSucceededMessage(String tableName){
        System.out.printf("\n%sInfo%s: se ha creado un nuevo registro en la tabla %s%s%s.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, tableName, Format.RESET_FORMAT);
    }


    //Operación de eliminación fallida.
    public void showInsertionFailedMessage(String tableName){
        System.out.printf("\n%sError%s: no ha sido posible crear el nuevo registro en la tabla %s%s%s.\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, tableName, Format.RESET_FORMAT);
    }


    //Operación de actualización ejecutada con éxito.
    public void showUpdateSucceededMessage(){
        System.out.printf("\n%sInfo%s: datos de registro actualizados con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
    }


    //Operación de eliminiación ejecutada con éxito.
    public void showDeletionSucceededMessage(){
        System.out.printf("\n%sInfo%s: datos de registro eliminados con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
    }


    //Operación de eliminación frustrada.
    public void showDeletionFailedMessage(){
        System.out.printf("\n%sError%s: no ha sido posible eliminar el registro.\n", Format.RED_COLOUR, Format.RESET_FORMAT);
    }


    //El registro ya existe.
    public void showMatchingRowMessage(String matchingColumn, String matchingValue){
        System.out.printf("\n%sError%s: ya existe un registro cuyo %s%s%s es %s%s%s.\n", Format.RED_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, matchingColumn, Format.RESET_FORMAT, Format.ITALIC_FONT, matchingValue, Format.RESET_FORMAT);
    }

    //El registro no existe.
    public void showNoMatchingRowMessage(String matchingColumn, String matchingValue){
        System.out.printf("\n%sInfo%s: no existe ningún registro cuyo %s%s%s sea %s%s%s.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, matchingColumn, Format.RESET_FORMAT, Format.ITALIC_FONT, matchingValue, Format.RESET_FORMAT);
    }


    //Mostrar mensaje de error.
    public void showWrongOptionMessage(int option, boolean showAsChar){
        if(showAsChar)
            System.out.printf("\n%sError%s: '%s' no es una opción válida.\n", Format.RED_COLOUR, Format.RESET_FORMAT, (char)option);
        else
            System.out.printf("\n%sError%s: '%d' no es una opción válida.\n", Format.RED_COLOUR, Format.RESET_FORMAT, option);
    }


    //Limpiar la vista de la consola.
    public void clearView(){

        ProcessBuilder processBuilder = new ProcessBuilder("clear");
        Process startProcess;

        try{
            startProcess = processBuilder.inheritIO().start();
            startProcess.waitFor();

        } catch(Exception e){
            e.printStackTrace();
        }
    }
}