package com.jerm.ecommerce;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class HibernateUtil {

    //Atributo estático para almacenar la instancia SessionFactory.
    private static SessionFactory sessionFactory;

    //Inicializador estático para instanciar el objeto SessionFactory del que se hará uso en la aplicación.
    static{
        Properties properties = loadProperties();
        Configuration configuration = new Configuration().addProperties(properties);
        configuration.addAnnotatedClass(com.jerm.ecommerce.Customer.class);
        configuration.addAnnotatedClass(com.jerm.ecommerce.Product.class);
        sessionFactory = configuration.buildSessionFactory();
        //Comentada a continuación, se muestra la forma recomendada de crear un objeto SessionFactory, de acuerdo
        //con las últimas directivas de Hibernate, que consideran obsoleto el método que yo he empleado. La razón
        //por la que hago uso del mismo es que, con la implementación actual, la ejecución de las aplicaciones del
        //arroja una org.hibernate.MappingException, motivada, según el portal StackOverflow, porque la versión de
        //Hibernate es demasiado antigua.
        //Adjunto enlace al artículo relacionado:
        //https://stackoverflow.com/questions/33194384/exception-in-thread-main-org-hibernate-mappingexception-unknown-entity/35587813
        //Configuration configuration = new Configuration().configure();
        //StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        //sessionFactory = configuration.buildSessionFactory(standardServiceRegistryBuilder.build());
    }


    //Constructor privado por defecto, según el patrón de diseño creacional Singleton.
    private HibernateUtil(){}


    //Método estático para obtener la instancia SessionFactory.
    public static SessionFactory getCurrentSessionFactory(){ return sessionFactory; }


    //Método estático para cargar las propiedades de conexión definidas en el archivo
    //hibernate.properties, así llamado por exigencias de framework de persistencia.
    private static Properties loadProperties(){

        String connectionProperties = "hibernate.properties";
        Properties properties = new Properties();

        try{
            properties.load(new FileReader(connectionProperties));
        } catch(IOException ioe){
            ioe.printStackTrace();
        }

        return properties;
    }
}