package com.jerm.ecommerce;


import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class MainClass {
    public static void main(String[] args){

        SessionFactory sessionFactory = HibernateUtil.getCurrentSessionFactory();
        Session session = sessionFactory.openSession();
        View view = new View();
        CustomerModel customerModel = new CustomerModel(session);
        ProductModel productModel = new ProductModel(session);
        Controller controller = new Controller(view, customerModel, productModel);
        controller.controllerMain();

        if(session != null)
            session.close();

        System.exit(0);
    }
}
