package com.jerm.ecommerce;

import java.util.List;
import java.util.Scanner;
import java.util.Vector;


public class Controller {

    //------- Atributos -------

    //Atributos asociados a esta instancia Controller
    private View view;
    private CustomerModel customerModel;
    private ProductModel productModel;
    private Customer customer;
    private Product product;
    private Scanner input;
    private int option; //Elección del usuario cada vez se le indica que elija una opción.


    //------- Constructores -------

    //Constructor general
    public Controller(View view, CustomerModel customerModel, ProductModel productModel){
        this.view = view;
        this.customerModel = customerModel;
        this.productModel = productModel;
        this.input = new Scanner(System.in);
    }


    //------- Métodos -------

    //Método que recrea la que sería la actividad propia del método main() de una clase ejecutable,
    //derivada aquí en el ámbito de este método para tratar de cumplir con el patrón de diseño MVC.
    public void controllerMain(){

        int chosenTable;


        while(true){

            this.view.showMainMenu();
            this.view.showChooseOptionMessage(4, true);
            this.option = input.nextInt();

            //Control de errores: el usuario es instado a seleccionar una opción válida (1,2,3,4,0).
            while(this.option < 0 || this.option > 4){
                this.view.showWrongOptionMessage(this.option, false);
                this.view.showChooseOptionMessage(4, true);
                this.option = input.nextInt();
            }


            switch(this.option){

                //Crear un nuevo registro
                case 1:
                    if(0 == (chosenTable = chooseTable()))
                        break;
                    if(1 == chosenTable)
                        newCustomer();
                    else
                        newProduct();
                    break;

                //Modificar registro.
                case 2:
                    if(0 == (chosenTable = chooseTable()))
                        break;
                    if(1 == chosenTable)
                        modifyCustomer();
                    else
                        modifyProduct();
                    break;

                //Eliminar registro.
                case 3:
                    if(0 == (chosenTable = chooseTable()))
                        break;
                    if(1 == chosenTable)
                        removeCustomer();
                    else
                        removeProduct();
                    break;

                //Consultar registro.
                case 4:
                    if(0 == (chosenTable = chooseTable()))
                        break;
                    if(1 == chosenTable)
                        getCustomerData();
                    else
                        getProductData();
                    break;

                //Abandonar la aplicación.
                case 0:
                    System.out.println();
                    System.exit(0);
            }

            this.view.showStayMessage();
            this.option = (int)input.next().toLowerCase().charAt(0);

            //Control de errores: el usuario es instado a seleccionar una opción válida (s,n).
            while((int)'s' != this.option && (int)'n' != this.option){
                this.view.showWrongOptionMessage(this.option, true);
                this.view.showStayMessage();
                this.option = (int)input.next().toLowerCase().charAt(0);
            }

            //Dentro del ámbito de este 'if', se da por terminada la ejecución del programa.
            if((int)'n'== this.option){
                System.out.println();
                System.exit(0);
            }

            //Si el usuario desea realizar otra operación, se limpia la consola y se vuelve a iterar sobre el bucle.
            this.view.clearView();
        }
    }


    //Mostrar al usuario el menú para que elija la tabla sobre la que desea operar.
    private int chooseTable(){

        this.view.showTablesMenu();
        this.view.showChooseOptionMessage(2,true);

        this.option = input.nextInt();

        while(this.option < 0 || this.option > 2){
            this.view.showWrongOptionMessage(this.option, false);
            this.view.showChooseOptionMessage(2, true);
            this.option = input.nextInt();
        }

        return this.option;
    }



    //Op.1a) Insertar datos en la tabla de clientes.
    private void newCustomer(){

        String email, username;
        List<Product> products;


        this.view.clearView();
        this.view.showTitle("\nregistro de nuevo cliente");

        this.view.showInsertDataMessage("Correo electrónico");
        input.nextLine(); //Vaciado del buffer del teclado.
        email = input.nextLine();

        this.view.showInsertDataMessage("Nombre de usuario");
        username = input.nextLine();

        products = setProductList();
        this.customer = new Customer(email, username, products);

        if(this.customerModel.insertCustomer(customer))
            this.view.showInsertionSucceededMessage("customer");
        else
            this.view.showMatchingRowMessage("email", customer.getEmail());
    }


    //Op.1b) Insertar datos en la tabla de productos.
    private void newProduct(){

        String productName;
        Double price;


        this.view.clearView();
        this.view.showTitle("\nregistro de nuevo producto");

        this.view.showInsertDataMessage("Nombre del producto");
        input.nextLine(); //Vaciado del 'buffer' del teclado.
        productName = input.nextLine();

        this.view.showInsertDataMessage("Precio");
        price = input.nextDouble();

        if (ProductModel.searchProduct(productName, this.productModel.getSession()) == null){

            this.product = new Product(productName, price, new Vector<>());
            setCustomerList();

            if(this.productModel.insertOrUpdateProduct(this.product))
                this.view.showInsertionSucceededMessage("product");
            else
                this.view.showInsertionFailedMessage("product");

        } else
            this.view.showMatchingRowMessage("nombre de producto", productName);
    }


    //Op.2a) Actualizar un registro de la tabla de clientes
    private void modifyCustomer(){

        String email, username;

        this.view.clearView();
        this.view.showTitle("\nmodificación de registro");

        this.view.showInsertDataMessage("Introduzca el " +
                                        Format.ITALIC_FONT + "email " + Format.RESET_FORMAT +
                                        "del cliente cuyos datos desea modificar");
        input.nextLine(); //Vaciado del 'buffer' de teclado.
        email = input.nextLine();

        if((this.customer = CustomerModel.searchCustomer(email, this.customerModel.getSession())) == null) {
            this.view.showNoMatchingRowMessage("email", email);
            return;
        }

        this.view.showInsertDataMessage("Nuevo correo electrónico");
        email = input.nextLine();
        this.customer.setEmail(email);

        this.view.showInsertDataMessage("Nuevo nombre de usuario");
        username = input.nextLine();
        this.customer.setUsername(username);

        this.customer.setProducts(setProductList());

        if(this.customerModel.updateCustomer(this.customer))
            this.view.showUpdateSucceededMessage();
        else
            this.view.showMatchingRowMessage("email", this.customer.getEmail());
    }


    //Op.2b) Actualizar un registro de la tabla de productos.
    private void modifyProduct(){

        String productName;
        Double price;

        this.view.clearView();
        this.view.showTitle("\nmodificación de registro");

        this.view.showInsertDataMessage("Introduzca el " +
                Format.ITALIC_FONT + "nombre " + Format.RESET_FORMAT +
                "del producto cuyos datos desea modificar");
        input.nextLine(); //Vaciado del 'buffer' de teclado.
        productName = input.nextLine();

        if((this.product = ProductModel.searchProduct(productName, this.productModel.getSession())) == null) {
            this.view.showNoMatchingRowMessage("nombre", productName);
            return;
        }

        this.view.showInsertDataMessage("Nuevo nombre del producto");
        productName = input.nextLine();
        this.product.setName(productName);

        this.view.showInsertDataMessage("Nuevo precio");
        price = input.nextDouble();
        input.nextLine(); //Vaciado del 'buffer' de teclado.
        this.product.setPrice(price);

        setCustomerList();

        if(this.productModel.insertOrUpdateProduct(this.product))
            this.view.showUpdateSucceededMessage();
        else
            this.view.showMatchingRowMessage("nombre", product.getName());
    }


    //Op.3a) Eliminar un cliente.
    private void removeCustomer(){

        String email;

        this.view.clearView();
        this.view.showTitle("\neliminiación de registro");

        this.view.showInsertDataMessage("Introduzca el " +
                Format.ITALIC_FONT + "email " + Format.RESET_FORMAT +
                "del cliente que desee eliminar");
        input.nextLine(); //Vaciado del 'buffer' de teclado.
        email = input.nextLine();

        if((this.customer = CustomerModel.searchCustomer(email, this.customerModel.getSession())) == null) {
            this.view.showNoMatchingRowMessage("email", email);
            return;
        }

        if(this.customerModel.deleteCustomer(this.customer))
            this.view.showDeletionSucceededMessage();
        else
            this.view.showDeletionFailedMessage();
    }


    //Op.3b) Eliminar un producto.
    private void removeProduct(){

        String productName;
        Double price;

        this.view.clearView();
        this.view.showTitle("\neliminiación de registro");

        this.view.showInsertDataMessage("Introduzca el " +
                Format.ITALIC_FONT + "nombre " + Format.RESET_FORMAT +
                "del producto que desee eliminar");
        input.nextLine(); //Vaciado del 'buffer' de teclado.
        productName = input.nextLine();

        if((this.product = ProductModel.searchProduct(productName, this.productModel.getSession())) == null) {
            this.view.showNoMatchingRowMessage("nombre", productName);
            return;
        }

        if(this.productModel.deleteProduct(this.product))
            this.view.showDeletionSucceededMessage();
        else
            this.view.showDeletionFailedMessage();
    }


    //Op.4a) Buscar un cliente.
    private void getCustomerData(){

        String email;

        this.view.clearView();
        this.view.showTitle("\nconsulta de registro");

        this.view.showInsertDataMessage("Introduzca el " +
                Format.ITALIC_FONT + "email " + Format.RESET_FORMAT +
                "del cliente cuyos datos desea consultar");
        input.nextLine(); //Vaciado del 'buffer' de teclado.
        email = input.nextLine();

        if((this.customer = CustomerModel.searchCustomer(email, this.customerModel.getSession())) == null) {
            this.view.showNoMatchingRowMessage("email", email);
            return;
        }

        this.view.showEntityData(this.customer);
    }


    //Op.4b) Consultar datos de un producto.
    private void getProductData(){

        String productName;

        this.view.clearView();
        this.view.showTitle("\nconsulta de registro");

        this.view.showInsertDataMessage("Introduzca el " +
                Format.ITALIC_FONT + "nombre " + Format.RESET_FORMAT +
                "del producto cuyos datos desea consultar");
        input.nextLine(); //Vaciado del 'buffer' de teclado.
        productName = input.nextLine();

        if((this.product = ProductModel.searchProduct(productName, this.productModel.getSession())) == null) {
            this.view.showNoMatchingRowMessage("nombre", productName);
            return;
        }

        this.view.showEntityData(this.product);
    }


    //A la hora de crear un cliente, se ha de dar al usuario la opción de especificar los productos
    //asociados al mismo. Y, de igual manera, hay que ofrecer esta posibilidad cuando se modifican los
    //datos de registro de un cliente, ya que, de otro modo, solo sería posible indicar los productos
    //adquiridos por un usuario del comercio electrónico en el momento en que se da de alta.
    private List<Product> setProductList(){

        List<Product> products = new Vector<>();
        this.product = null;
        String productName;
        Double price;
        boolean namesMatch;

        this.view.showYesOrNoMessage("Desea agregar algún producto a este cliente");
        this.option = input.next().toLowerCase().charAt(0);

        //Repetición del bucle mientras el usuario desee incorporar productos al cliente creado.
        while(true) {

            while ((int)'s' != this.option && (int)'n' != this.option) {
                this.view.showWrongOptionMessage(this.option, true);
                this.view.showYesOrNoMessage("Desea agregar algún producto a este cliente");
                this.option = input.next().toLowerCase().charAt(0);
            }

            if ((int)'n' == this.option)
                break;

            this.view.showInsertDataMessage("\nNombre del producto");
            input.nextLine(); //Vaciado del buffer del teclado.
            productName = input.nextLine();

            this.view.showInsertDataMessage("Precio");
            price = input.nextDouble();

            //Primera medida para prevenir que se introduzcan productos con nombre repetido. Si bien a la hora
            //de registrar cada nuevo cliente se comprueba que el nombre de ninguno de sus productos coincida
            //con los que ya hay en la base de datos, cuando la base de datos se encuentra vacía, no hay coincidencia
            //posible, y puesto que los productos solo se incorporan al contexto de persistencia cuando lo hace el
            //cliente, nada impediría que se diese de alta un conjunto de productos con el mismo nombre.
            this.product = new Product(productName, price, new Vector<>());
            namesMatch = false;

            for(int i=0; i<products.size() && !namesMatch; i++)
                if(products.get(i).getName().compareToIgnoreCase(this.product.getName()) == 0)
                    namesMatch = true;

            if(!namesMatch)
                products.add(this.product);

            this.view.showYesOrNoMessage("Desea agregar algún otro producto a este cliente");
            this.option = input.next().toLowerCase().charAt(0);
        }

        return products;
    }


    //Establecer la lista de clientes asociada al producto que se modifica o que se crea.
    //Este método devuelve un producto después de haber definido su lista de clientes.
    private void setCustomerList(){

        String email, username;
        boolean emailsMatch;
        List<Product> products = new Vector<>();
        products.add(this.product);


        this.view.showYesOrNoMessage("Desea agregar algún cliente a este producto");
        this.option = input.next().toLowerCase().charAt(0);

        //Repetición del bucle mientras el usuario desee incorporar clientes al producto creado.
        while(true) {

            while ((int)'s' != this.option && (int)'n' != this.option) {
                this.view.showWrongOptionMessage(this.option, true);
                this.view.showYesOrNoMessage("Desea agregar algún cliente a este producto");
                this.option = input.next().toLowerCase().charAt(0);
            }

            if ((int)'n' == this.option)
                break;

            this.view.showInsertDataMessage("\nCorreo electrónico");
            input.nextLine(); //Vaciado del 'buffer' del teclado.
            email = input.nextLine();

            this.view.showInsertDataMessage("Nombre de usuario");
            username = input.nextLine();

            emailsMatch = false;

            for(int i=0; i<this.product.getCustomers().size() && !emailsMatch; i++)
                if(this.product.getCustomers().get(i).getEmail().compareToIgnoreCase(email) == 0)
                    emailsMatch = true;

            if(!emailsMatch)
                this.product.getCustomers().add(new Customer(email, username, products));

            this.view.showYesOrNoMessage("Desea agregar algún otro cliente a este producto");
            this.option = input.next().toLowerCase().charAt(0);
        }
    }
}