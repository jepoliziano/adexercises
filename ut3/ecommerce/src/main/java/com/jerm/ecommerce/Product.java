package com.jerm.ecommerce;

import javax.persistence.*;
import java.util.List;
import java.util.Vector;


@Entity
@Table(name="product", schema="public", catalog="ecommerce")
public class Product implements java.io.Serializable{

    //------- Atributos -------

    @Id
    @Column(name="product_id")
    @GeneratedValue(generator="productIdGenerator")
    @SequenceGenerator(name="productIdGenerator", sequenceName="seq_product_id", allocationSize=1)
    private int code;

    @Column(name="product_name")
    private String name;

    @Column(name="product_price")
    private Double price;

    @ManyToMany(mappedBy="products", cascade=CascadeType.ALL)
    private List<Customer> customers;


    //------- Constructores ------

    //Constructor por defecto
    public Product(){
        this.customers = new Vector<>();
    }

    //Constructor general
    public Product(String name, Double price, List<Customer> customers){
        this.name = name;
        this.price = price;
        this.customers = customers;
    }


    //------- Métodos 'setter' y 'getter' -------

    public Integer getCode(){ return this.code; }

    public String getName(){ return this.name; }
    public void setName(String name){ this.name = name;}

    public void setPrice(Double price){ this.price = price;}
    public Double getPrice(){ return this.price; }

    public void setCustomers(List<Customer> customers){ this.customers = customers; }
    public List<Customer> getCustomers(){ return this.customers; }


    //------- Otros métodos -------

    //Sobrescritura del método toString() de Object.
    @Override public String toString(){
        return "Id: " + Format.CYAN_COLOUR + this.code + Format.RESET_FORMAT + "\n" +
               "Nombre: " + this.name + "\n" +
               "Precio: " + this.price;
               //"Compradores: " + this.customers;
    }
}
