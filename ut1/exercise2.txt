2) Investiga la viabilidad e inconvenientes del cambio de las propiedades del sistema desde nuestras aplicaciones Java, mediante consulta en Internet.

    Respecto de la viabilidad de modificar las propiedades del sistema desde un programa Java, en efecto, este lenguaje proporciona las clases que permitirían llevar a cabo esta operación. El código a continuación, por ejemplo, cambiaría el nombre del sistema operativo a Windows; así como establecería el carácter que separa los nodos o componentes de la ruta de un archivo para que coincidiese con el del SO de Microsoft ("\"). En última instancia, imprimiría por consola todas las propiedades del sistema, para constatar los cambios realizados.

/**
 * import java.util.*;
 * 
 * public class ChangeSystemProperties{
 * 
 *      public static void main(String[] args){
 * 
 *          Properties props = System.getProperties();
 *          props.setProperty("os.name", "Windows");
 *          props.setProperty("file.separator", "\\");
 *          System.setProperties(props);
 *          props.list(System.out);
 *      }
 * }
 */

    Presumiblemente, este pequeño fragmento de código debería causar desbarjustes en el sistema. Sin embargo, tras haberlo ejecutado sobre una máquina virtual como 'root', tengo que decir que no he percibido ningún cambio. Después de indagar en la red sin éxito, solo puedo concluir que, o bien hay alguna carencia en el código o existe alguna medida de seguridad para proteger el sistema de acciones que, como esta, representan una amenaza potencial para su normal funcionamiento.
