
//package mvcexercise;



public class App{
	
	public static void main(String[] args) {

		AppView view = new AppView();
		AppModel model = new AppModel();
		AppController controller = new AppController(view, model);
	}
}