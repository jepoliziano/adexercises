
//package mvcexercise;

import java.util.ArrayList;



public class AppModel{
	

	//------- Atributos -------
	private ArrayList<Animal> animals;


	//------- Constructores -------

	//Constructor por defecto
	public AppModel(){ this.animals = new ArrayList<>(); }


	//------- Métodos -------


	//Buscar un animal en la lista.
	public Animal searchAnimal(String name){

		if(animals.isEmpty())
			return null;

		for(Animal animal: animals)
			if(animal.getName().compareToIgnoreCase(name) == 0)
				return animal;

		return null;
	}


	//Agregar un nuevo animal a la lista, previa comprobación de que no existe uno con el mismo nombre.
	public boolean saveAnimal(Animal newAnimal){ 
		
		if(this.searchAnimal(newAnimal.getName()) == null){	
			this.animals.add(newAnimal);
			return true;
		}

		return false;
	}


	/*
	  Modificar un animal, después de asegurarse de que el nombre con que se pretende
	  almacenar no coincide con el de alguno de los animales ya almacenados.
	*/
	public boolean modifyAnimal(Animal replacementAnimal, String nameTxtFldContents){

		if(this.searchAnimal(replacementAnimal.getName()) == null){

			for(int i=0; i<animals.size(); i++)
				if(animals.get(i).getName().compareToIgnoreCase(nameTxtFldContents) == 0){
					animals.set(i, replacementAnimal);
					return true;
				}
		}

		return false;
	}


	//Eliminar un animal
	public void removeAnimal(String name){

		for(int i=0; i<animals.size(); i++)
			if(animals.get(i).getName().compareTo(name) == 0){
				animals.remove(i);
				break;
			}
	}


	//Devolver los datos del primer animal registrado, si lo hubiere.
	public Animal getFirst(){

		if(animals.isEmpty())
			return null;

		return animals.get(0);
	}


	/*
	  Dada la posición en el ArrayList del objeto Animal pasado como parámetro,
	  este método devuelve el que hay almacenado inmedatamente a continuación.
	*/
	public Animal getPrevious(Animal animal){

		for(int i=animals.size()-1; i>0; i--)
			if(animals.get(i).getName().compareToIgnoreCase(animal.getName()) == 0)
				return animals.get(i-1);

		return null;
	}


	/*
	  Dada la posición en el ArrayList del objeto Animal pasado como parámetro,
	  este método devuelve el que hay almacenado inmedatamente antes.
	*/
	public Animal getNext(Animal animal){

		for(int i=0; i<animals.size() - 1; i++)
			if(animals.get(i).getName().compareToIgnoreCase(animal.getName()) == 0)
				return animals.get(i+1);

		return null;
	}


	//Devolver los datos del último animal registrado, si los hubiere.
	public Animal getLast(){

		if(animals.isEmpty())
			return null;

		return animals.get(animals.size() - 1);
	}
}