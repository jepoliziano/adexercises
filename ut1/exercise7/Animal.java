
//package mvcexercise;



public class Animal{
	

	//------- Atributos -------
	private String name;
	private String breed;
	private String features;
	private double weight;


	//------- Constructores -------

	//Constructor general
	public Animal(String name, String breed, String features, double weight){
		this.name = name;
		this.breed = breed;
		this.features = features;
		this.weight = weight;
	}

	//Constructor de copia
	public Animal(Animal a){
		this.name = a.name;
		this.breed = a.breed;
		this.features = a.features;
		this.weight = a.weight;
	}


	//------- Métodos 'setter' y 'getter' -------
	public void setName(String name){ this.name = name; }
	public String getName(){ return this.name; }

	public void setBreed(String breed){ this.breed = breed; }
	public String getBreed(){ return this.breed; }

	public void setFeatures(String features){ this.features = features; }
	public String getFeatures(){ return this.features; }

	public void setWeight(double weight){ this.weight = weight; }
	public double getWeight(){ return this.weight; }

	//------- Otros métodos -------

	//Sobrescritura del método toString() definido en la clase Object.
	public String toString(){

		return "Nombre: " + this.name +
			   "\nRaza: " + this.breed +
			   "\nCaracterísticas: " + this.features +
			   "\nPeso: " + this.weight;
	}
}