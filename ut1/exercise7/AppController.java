
//package mvcexercise;

import java.util.Scanner;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;



public class AppController implements ActionListener{
	
	//------- Atributos -------
	private AppView view;
	private AppModel model;
	private Animal animal;
	//A partir de este 'booleano', se determina si la opción de modificación está o no habilitada.
	private static boolean isModificationEnabled;
	/*
	  Cada vez que se refrescan los datos de la interfaz, esta cadena almacena el último nombre visible en
	  el campo de texto para utilizarlo como criterio de comparación en caso de modificar la información
	  del animal.
	*/
	private static String nameTxtFldContents;
	private static String actionCommand; //Texto que dientifica los botones de la interfaz.
	/*
	  NOTA: la del inicializador estático es, en realidad, una medida innecesaria, pues, si no se indica lo contrario,
	  la variable booleana toma por defecto el valor de 'false'.
	*/
	static{ isModificationEnabled = false; }


	//------- Constructores -------

	//Constructor por defecto
	public AppController(AppView view, AppModel model){

		this.view = view;
		this.model = model;
		Component[] buttons = this.view.buttonsPanel.getComponents();
		/*
		  A través de este bucle, se añade a todos los botones del buttonsPanel el gestor de eventos
		  impuesto por la interfaz ActionListener implementada.
		*/
		for(Component btn: buttons)
			((JButton)btn).addActionListener(this);

		this.view.searchBtn.addActionListener(this);
	}


	//------- Métodos -------

	/*
	  Sobrescritura del método actionPerformed() exigido por la interfaz ActionListener, dentro de cuyo
	  ámbito se describe el comportamiento de los botones de la interfaz.
	*/
	public void actionPerformed(ActionEvent e){

		actionCommand = e.getActionCommand();

		switch(actionCommand){

			case "Guardar":
				this.saveAnimal();
				break;

			case "Nuevo":
				this.clearTxtFlds();
				break;

			case "Modificar":
				this.modifyAnimal();
				break;

			case "Buscar":
				this.searchAnimal();
				break;

			case "Borrar":
				this.removeAnimal();
				break;

			case "|<":
				this.getFirst();
				break;

			case "<":
				this.getPrevious();
				break;

			case ">":
				this.getNext();
				break;

			case ">|":
				this.getLast();
				break;
		}
		/*
		  Invocando a este método estático, después de pulsar cada botón, se determina si la opción de modificación debe
		  estar o no habilitada, según la interfaz contenga información en este momento o esté vacía, respectivamente.
		*/
		setModificationStatus();
	}


	//Operaciones desencadenadas por el botón "Guardar".
	private void saveAnimal(){

		//Si el formulario está vacío, la pulsación del botón no tiene efecto.
		if((animal = this.getFormData()) == null)
			return;

		if(model.saveAnimal(animal))
			showMessageDialog("Nuevo animal registrado con éxito.", true);
		else
			showMessageDialog("Ya existe un animal llamado " + animal.getName(), false);

		this.clearTxtFlds();
	}


	//El botón "Nuevo", sencillamente, despeja los campos de texto de la interfaz.
	private void clearTxtFlds(){

		Component[] txtFlds = view.dataPanel.getComponents();

		for(Component component: txtFlds)
			if(component instanceof JTextField)
				((JTextField)component).setText("");
	}


	//Operaciones desencadenadas al puslar el botón "Modificar".
	private void modifyAnimal(){

		if((animal = this.getFormData()) == null || !isModificationEnabled || animal.getName().compareToIgnoreCase(nameTxtFldContents) == 0)
			return;

		if(this.model.modifyAnimal(animal, nameTxtFldContents))
			this.showMessageDialog("Datos de registro modificados con éxito.", true);
		else
			this.showMessageDialog("Ya existe un animal llamado " + animal.getName(), false);
	}


	//Operaciones desencadenadas por el bóton de 'Borrar'.
	private void removeAnimal(){

		String name = this.view.nameTxtFld.getText();

		if(name.compareTo("") == 0)
			return;

		if(this.model.searchAnimal(name) == null)
			showMessageDialog("No existe ningún animal llamado " + name, false);

		else if(JOptionPane.showConfirmDialog(null,
											  "¿Está seguro de que desea eliminar a " + name + " del registro?",
											  "Eliminar",
											  JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){

			this.model.removeAnimal(name);
			showMessageDialog(name + " ha sido eliminado del registro.", true);
			this.clearTxtFlds();
		}
	}


	//Operaciones desencadenadas por la pulsación del botón de búsqueda.
	public void searchAnimal(){

		String name = this.view.searchTxtFld.getText();

		if(name.compareTo("") == 0)
			return;
		
		if((animal = this.model.searchAnimal(name)) == null){	
			showMessageDialog("No existe ningún animal llamado " + name, true);
			return;
		}
			
		this.setFormData(animal);
	}


	//Mostrar los datos del primer animal registrado en el ArrayList definido en el modelo.
	private void getFirst(){

		if((animal = this.model.getFirst()) == null)
			return;

		this.setFormData(animal);
	}


	//Mostrar los datos del animal almacenado en el ArrayList del modelo antes del animal que figura en la interfaz.
	private void getPrevious(){

		if((animal = this.getFormData()) == null){
			this.getLast();
			return;
		}
		/*
		  Antes de invocar al método que muestra el animal anterior, se establece que el nombre del que se envía como
		  parámetro sea el que figuraba en el formulario como resultado de la llamada al método de búsqueda a alguno
		  de los de desplazamiento. De esta manera, por ejemplo, si después de pulsar el botón que muestra el primer
		  animal almacenado, se altera el nombre en el formulario, al pulsar la tecla '>', aparecerá el animal siguiente,
		  sin que el texto visible en este momento interfiera.
		*/
		animal.setName(nameTxtFldContents);
		
		if((animal = this.model.getPrevious(animal)) == null)
			return;

		this.setFormData(animal);
	}


	//Mostrar los datos del animal almacenado en el ArrayList del modelo a continuación del animal que figura en la interfaz.
	private void getNext(){

		if((animal = this.getFormData()) == null){
			this.getFirst();
			return;
		}

		animal.setName(nameTxtFldContents);

		if((animal = this.model.getNext(animal)) == null)
			return;

		this.setFormData(animal);
	}


	//Mostrar los datos del último animal almacenado en el ArrayList definido en el modelo.
	private void getLast(){

		if((animal = this.model.getLast()) == null)
			return;

		this.setFormData(animal);
	}


	/*
	  Después de cada acción ejecutada por los botones de la interfaz, este método determina si la opción de
	  modificación debe habilitarse o no. Así, por ejemplo, al pulsar 'Nuevo', los datos del formulario se desvanecen,
	  perdiendo cualquier referencia con que buscar coincidencias en el ArryList del modelo que almacena los animales
	  registrados. En cambio, al interactuar con los botones de desplazamiento, que rellenan los campos del formulario
	  con la información de los animales registrados, cabe alterar los datos de cualquiera de ellos, gracias a que,
	  con cada pulsación, la variable estática nameTxtFldContents toma el valor del texto que figura en el campo del
	  nombre y lo utiliza como criterio de búsqueda para reemplazar los datos de dicho animal con los que se introdujese
	  antes de pulsar en 'Modificar'.
	*/
	private void setModificationStatus(){

		nameTxtFldContents = this.view.nameTxtFld.getText();

		if(this.model.searchAnimal(nameTxtFldContents) == null){
			isModificationEnabled = false;
			return;
		}

		isModificationEnabled = true;
	}


	/*
	  Este método se ocupa de recuperar el texto de los campos JTextField del formulario para todas las
	  operaciones desencadenadas por los botones de la interfaz cada vez que sea requerido.
	*/
	private Animal getFormData(){

		String name = this.view.nameTxtFld.getText(),
			   breed = this.view.breedTxtFld.getText(),
			   features = this.view.featuresTxtFld.getText(),
			   weightStr = this.view.weightTxtFld.getText();
		double weight = (weightStr.compareTo("") != 0 && weightStr.compareTo("Sin especificar") != 0)
						? Double.parseDouble(weightStr)
						: 0;

		if(name.compareTo("") == 0){

			if(actionCommand.compareTo("<") == 0 || actionCommand.compareTo(">") == 0)
				return null;

			showMessageDialog("El nombre es un campo obligatorio.", false);
			return null;
		}

		if(breed.compareTo("") == 0){
			showMessageDialog("La raza es un campo obligatorio.", false);
			return null;
		}

		return new Animal(name, breed, features, weight);
	}


	/*
	  Paralelamente al método anterior, este se ocupa de rellenar los campos del formulario
	  con los datos del animal recibido como parámetro.
	*/
	private void setFormData(Animal animal){

		String features,
			   weight;
		/*
		  Si el animal ha sido almacenado sin indicar la 'raza' o 'peso', los campos correspondientes
		  del formulario mostrarán que se trata de datos 'Sin especificar'.
		*/
		if((features = animal.getFeatures()).compareTo("") == 0)
			features = "Sin especificar";
		
		if((weight = String.valueOf(animal.getWeight())).compareTo("0.0") == 0)
			weight = "Sin especificar";

		this.view.nameTxtFld.setText(animal.getName());
		this.view.breedTxtFld.setText(animal.getBreed());
		this.view.featuresTxtFld.setText(features);
		this.view.weightTxtFld.setText(weight);
	}


	/*
	  Este método muestra los mensajes que comunican al usuario el éxito o el error de las oepraciones
	  ejecutadas. Nótese que el nombre de este método coincidie con el que define la clase JOptionPane.
	  Sin embargo, aquel es de tipo estático, mientras que este ha sido declarado en el ámbito de la clase
	  AppController, lo cual no representa un error.
	*/
	private void showMessageDialog(String messageDialog, boolean success){

		if(success)
			JOptionPane.showMessageDialog(null,
										  messageDialog,
										  "Información",
										  JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(null,
										  messageDialog,
										  "Error",
										  JOptionPane.ERROR_MESSAGE);
	}
}