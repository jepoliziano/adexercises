
//package mvcexercise;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;



public class AppView extends JFrame{

	//------- Atributos asociados a los componentes de la interfaz -------
	protected JPanel dataPanel;
	protected JPanel buttonsPanel;
	protected JPanel searchPanel;
	protected JTextField nameTxtFld;
	protected JTextField breedTxtFld;
	protected JTextField featuresTxtFld;
	protected JTextField weightTxtFld;
	protected JTextField searchTxtFld;
	private JLabel nameLabel;
	private JLabel breedLabel;
	private JLabel featuresLabel;
	private JLabel weightLabel;
	protected JButton newBtn;
	protected JButton saveBtn;
	protected JButton modifyBtn;
	protected JButton removeBtn;
	protected JButton firstBtn;
	protected JButton nextBtn;
	protected JButton previousBtn;
	protected JButton lastBtn;
	protected JButton searchBtn;


	//------- Constructores -------

	//Constructor por defecto
	public AppView(){

		//Instanciación de componentes
		super("Gestor de animales");
		this.dataPanel = new JPanel(new GridLayout(4,2,7,7));
		this.buttonsPanel = new JPanel(new GridLayout(2,4,7,7));
		this.searchPanel = new JPanel(new FlowLayout());
		this.nameTxtFld = new JTextField("");
		this.breedTxtFld = new JTextField("");
		this.featuresTxtFld = new JTextField("");
		this.weightTxtFld = new JTextField("");
		this.searchTxtFld = new JTextField("", 16);
		this.nameLabel = new JLabel("Nombre*: ", SwingConstants.LEFT);
		this.breedLabel = new JLabel("Raza*: ", SwingConstants.LEFT);
		this.featuresLabel = new JLabel("Características: ", SwingConstants.LEFT);
		this.weightLabel = new JLabel("Peso: ", SwingConstants.LEFT);
		this.newBtn = new JButton("Nuevo");
		this.saveBtn = new JButton("Guardar");
		this.modifyBtn = new JButton("Modificar");
		this.removeBtn = new JButton("Borrar");
		this.firstBtn = new JButton("|<");
		this.nextBtn = new JButton("<");
		this.previousBtn = new JButton(">");
		this.lastBtn = new JButton(">|");
		this.searchBtn = new JButton("Buscar");

		//Composición de la interfaz
		this.getContentPane().add(dataPanel);
		this.getContentPane().add(buttonsPanel);
		this.getContentPane().add(searchPanel);
		this.dataPanel.add(nameLabel);
		this.dataPanel.add(nameTxtFld);
		this.dataPanel.add(breedLabel);
		this.dataPanel.add(breedTxtFld);
		this.dataPanel.add(featuresLabel);
		this.dataPanel.add(featuresTxtFld);
		this.dataPanel.add(weightLabel);
		this.dataPanel.add(weightTxtFld);
		this.buttonsPanel.add(newBtn);
		this.buttonsPanel.add(saveBtn);
		this.buttonsPanel.add(modifyBtn);
		this.buttonsPanel.add(removeBtn);
		this.buttonsPanel.add(firstBtn);
		this.buttonsPanel.add(nextBtn);
		this.buttonsPanel.add(previousBtn);
		this.buttonsPanel.add(lastBtn);
		this.searchPanel.add(searchTxtFld);
		this.searchPanel.add(searchBtn);

		//Aspectos estéticos
		this.setLayout(new GridLayout(0,1,7,7));
		this.dataPanel.setBorder(BorderFactory.createLineBorder(new Color(238,238,238), 7));
		this.buttonsPanel.setBorder(BorderFactory.createLineBorder(new Color(238,238,238), 7));
		this.searchPanel.setBorder(BorderFactory.createLineBorder(new Color(238,238,238), 7));

		//Aspecetos funcionales
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(super.EXIT_ON_CLOSE);
		this.setResizable(false);

		this.pack();
	}
}