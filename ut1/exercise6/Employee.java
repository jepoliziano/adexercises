
public class Employee{
	

	//------- Atributos -------

	//Atributos estáticos
	private static int MAX_STRING;

	static{ MAX_STRING = 44; }

	//Atributos asociados a las instancias de la clase
	private String dni;
	private String name;
	private String surnames;


	//------- Constructores -------

	//Constructor general
	public Employee(String dni, String name, String surnames){

		if(name.length() > MAX_STRING)
			name = name.substring(0, MAX_STRING);
		
		if(surnames.length() > MAX_STRING)
			surnames = surnames.substring(0, MAX_STRING);

		this.dni = generateNIF(dni);
		this.name = name;
		this.surnames = surnames;
	}

	//Constructor de copia
	public Employee(Employee e){
		this.dni = e.dni;
		this.name = e.name;
		this.surnames = e.surnames;
	}


	//------- Métodos 'setter' y 'getter' -------
	public void setDNI(String dni){ this.dni = generateNIF(dni); }
	public String getDNI(){ return this.dni; }

	public void setName(String name){ this.name = name; }
	public String getName(){ return this.name; }

	public void setSurnames(String surnames){ this.surnames = surnames; }
	public String getSurnames(){ return this.surnames; }



	//------- Métodos adicionales -------

	//Sobrescritura del método toString() de Object.
	@Override public String toString(){ return "DNI: " + Format.CYAN_COLOUR + this.dni + Format.RESET_FORMAT + "\nNombre: " + this.name + "\nApellidos: " + this.surnames; }

	//Método para generar/validar el NIF a partir del DNI recibido como parámetro.
	public static String generateNIF(String dni){

		int position;
		char letter;
		String lettersSequence = "TRWAGMYFPDXBNJZSQVHLCKE";
		
		dni = dni.substring(0,8);
		position = Integer.parseInt(dni) % 23;
		letter = lettersSequence.charAt(position);
		dni += letter;
		
		return dni;
	}
}