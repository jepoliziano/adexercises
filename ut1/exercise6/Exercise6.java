/*
  6) Realiza una aplicación que, mediante RandomAccessFile, genere un fichero con datos de empleados de una empresa.
  La posición ocupada por cada empleado en el fichero vendrá determinada por las 3 últimas cifras de su DNI.
  Podrá haber sinónimos. Resuélvelo con un área de excedentes al final del fichero.
*/

import java.io.*;
import java.util.Scanner;




class Format{
	
	public static String RED_COLOUR;
	public static String GREEN_COLOUR;
	public static String CYAN_COLOUR;
	public static String ITALIC_FONT;
	public static String RESET_FORMAT;

	static{
		RED_COLOUR = "\033[31m";
		GREEN_COLOUR = "\033[32m";
		CYAN_COLOUR = "\033[36m";
		ITALIC_FONT = "\033[3m";
		RESET_FORMAT = "\u001B[0m";
	}
}




public class Exercise6{


	//------- Atributos -------

	private static int ITEM_SIZE; //Tamaño de cada uno de los elementos a almacenar en el archivo.
	private static int INIT_FILE_SIZE; //Tamaño inicial del archivo de acceso aleatorio.
	private static String FILE_NAME; //Nombre del archivo.
	private static String inputDNI;
	private static Employee employee;
	/*
	  Definido como atributo de la clase, el archivo de acceso aleatorio será accesible para todos
	  los métodos declarados en el ámbito de la misma, lo cual facilita las operaciones a realizar.
	*/
	private static RandomAccessFile sourceRAF;
	private static Scanner input;

	static{
		ITEM_SIZE = 100;
		INIT_FILE_SIZE = ITEM_SIZE * 1000;
		FILE_NAME = "employees.dat";
		employee = null;
		input = new Scanner(System.in);
	}



	//------- Métodos -------

	//Método 'main'.
	public static void main(String[] args) throws FileNotFoundException, IOException{


		int option;
		ProcessBuilder pb = new ProcessBuilder("clear");
		Process startProcess = null;


		while(true){
			/*
			  El bucle que aquí comienza se repetirá mientras el usuario desee realizar operaciones sobre el registro
			  de empleados. Cada iteración se corresponde con una operación de las que ofrece el menú. Colocando al
			  comienzo del bucle la instanciación del objeto que representa el archivo de acceso aleatorio, se consigue
			  actualizar su contenido después de cada operación ejecutada para reflejar los cambios realizados.
			*/
			sourceRAF = new RandomAccessFile(FILE_NAME, "rw");
			/*
			  Si el archivo de acceso aleatorio no existiese antes de la ejecución del programa, ante todo, hay que crearlo
			  y dotarlo de un tamaño inicial suficiente como para almacenar un total de (DNI % 1000) empleados, siendo DNI
			  un número entero de ocho dígitos. De esta manera, se evitarán las excepciones de tipo EndOfFileException que
			  saltarían al tratar de leer posiciones del archivo por encima del tamaño del mismo durante las operacioens de
			  lectura y/o escritura.
			*/
			if(0 == sourceRAF.length())
				sourceRAF.setLength(INIT_FILE_SIZE);


			System.out.println(Format.RESET_FORMAT);
			System.out.println("   **********************************");
			System.out.println("   *               MENÚ             *");
			System.out.println("   **********************************");
			System.out.println("   *                                *");
			System.out.println("   * 1) Registrar nuevo empleado.   *");
			System.out.println("   * 2) Dar de baja empleado.       *");
			System.out.println("   * 3) Buscar empleado.            *");
			System.out.println("   * 0) Salir.                      *");
			System.out.println("   *                                *");
			System.out.println("   **********************************");

			System.out.print("\nSeleccione una opción del menú superior: [1,2,3,0] " + Format.CYAN_COLOUR);
			option = input.nextInt();

			//Control de errores: el usuario es instado a seleccionar una opción válida (1,2,3,0).
			while(option < 0 || option > 3){
				System.out.printf("\n%sError%s: '%d' no es una opción válida.\n", Format.RED_COLOUR, Format.RESET_FORMAT, option);
				System.out.print("\nSeleccione una opción del menú superior: [1,2,3,0] " + Format.CYAN_COLOUR);
				option = input.nextInt();
			}


			switch(option){

				//Dar de alta un nuevo empleado.
				case 1:
					newEmployee();					
					break;


				//Dar de baja un empleado.
				case 2:
					removeEmployee();
					break;


				//Buscar empleado.
				case 3:
					searchEmployee();
					break;


				//Abandonar la aplicación.
				case 0:
					sourceRAF.close();
					System.out.println();
					System.exit(0);
			}


			System.out.print(Format.RESET_FORMAT + "\n¿Desea realizar alguna otra operación? [s,n] " + Format.CYAN_COLOUR);
			option = (int)input.next().toLowerCase().charAt(0);

			//Control de errores: el usuario es instado a seleccionar una opción válida (s,n).
			while((int)'s' != option && (int)'n' != option){
				System.out.printf("\n%sError%s: '%s' no es una opción válida.\n", Format.RED_COLOUR, Format.RESET_FORMAT, (char)option);
				System.out.print("\n¿Desea realizar alguna otra operación? [s,n] " + Format.CYAN_COLOUR);
				option = (int)input.next().toLowerCase().charAt(0);
			}

			//Dentro del ámbito de este 'if', se da por terminada la ejecución del programa.
			if((int)'n'== option){
				sourceRAF.close();
				System.out.println();
				System.exit(0);
			}
			
			//Si el usuario desea realizar otra operación, se limpia la consola y se vuelve a iterar sobre el bucle.
			try{
				startProcess = pb.inheritIO().start();
				startProcess.waitFor();

			} catch(Exception e){
				e.printStackTrace();
			}
		}
	}



	//------- Procedimientos desencadenados por las opciones del menú -------

	//Nuevo empleado
	private static void newEmployee() throws IOException{
		writeIntoRAF(instanceNewEmployee());
		System.out.printf("\n%sInfo%s: nuevo empleado registrado con éxito.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
	}


	//Eliminar empleado
	private static void removeEmployee() throws IOException{

		String removedEmployee;

		System.out.print(Format.RESET_FORMAT + "\nIntroduzca el DNI del empleado que desea eliminar: " + Format.CYAN_COLOUR);
		inputDNI = input.next();

		if((removedEmployee = removeFromRAF(inputDNI)).compareTo("") != 0)
			System.out.printf("\n%sInfo%s: se ha eliminado a %s%s%s del registro.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT, Format.ITALIC_FONT, removedEmployee, Format.RESET_FORMAT);
		else
			System.out.printf("\n%sInfo%s: no existe ningún empleado con el DNI especificado.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
	}


	//Buscar empleado
	private static void searchEmployee() throws IOException{

		System.out.print(Format.RESET_FORMAT + "\nIntroduzca el DNI del empleado cuyos datos desea consultar: " + Format.CYAN_COLOUR);
		inputDNI = input.next();
		
		if((employee = searchInRAF(inputDNI)) == null)
			System.out.printf("\n%sInfo%s: no existe ningún empleado con el DNI especificado.\n", Format.GREEN_COLOUR, Format.RESET_FORMAT);
		else{
			System.out.println(Format.RESET_FORMAT);
			System.out.println("DATOS DE REGISTRO");
			System.out.println("-----------------");
			System.out.println(employee);
		}
	}



	//------- Métodos relacionados con las operaciones que actúan sobre el RAF -------

	//Método para registrar un nuevo empleado en el fichero RandomAccessFile.
	private static void writeIntoRAF(Employee employee) throws IOException{

		int position = calculatePosition(employee.getDNI());
		byte[] dni = new byte[10]; //Array de bytes para almacenar el DNI leído en el archivo.

		sourceRAF.seek(position);
		/*
		  El método read() carga en el array pasado como parámetro un número de bytes equivalente a la longitud
		  del mismo, a partir de la posición actual del puntero del archivo de acceso aleatorio.
		*/
		sourceRAF.read(dni);
		/*
		  Posicionado el puntero en la instancia RandomAccessFile, se leen los diez bytes del archivo que
		  corresponderían al DNI del empleado almacenado en ese punto, si lo hubiere. Puesto que al final
		  de cada uno de los datos guardados para cada empleado se escribe el carácter de salto de línea,
		  basta con comprobar si dicho carácter es el último elemento del vector de bytes para verificar
		  si en la posición dada ya hay registrado un empleado.
		*/
		if(10 == dni[dni.length - 1]){
			sourceRAF.seek(sourceRAF.length());
			sourceRAF.writeBytes(employee.getDNI() + "\n" + employee.getName() + "\n" + employee.getSurnames() + "\n");
		
		} else{
			//Después de la llamada al método read(), es necesario volver a posicionarse en el archivo.
			sourceRAF.seek(position);
			sourceRAF.writeBytes(employee.getDNI() + "\n" + employee.getName() + "\n" + employee.getSurnames() + "\n");
		}
	}



	//Buscar un empleado a partir de su DNI.
	private static Employee searchInRAF(String searchedDNI) throws IOException{

		searchedDNI = Employee.generateNIF(searchedDNI);
		int position = calculatePosition(searchedDNI);
		byte[] readDNI = new byte[10];
		String readDNItoString = "",
			   name,
			   surnames;

		//Posicionamiento en el objeto RAF.
		sourceRAF.seek(position);
		sourceRAF.read(readDNI);

		//Ante todo, hay que comprobar si existe el empleado requerido en el espacio de acceso aleatorio del fichero.
		if(10 == readDNI[readDNI.length - 1]){
			/*
			  El límite del bucle ha de establecerse en (readDNI.length-1) para evitar que en la comparación de
			  cadenas intervenga el carácter de salto de línea.
			*/
			for(int i=0; i<readDNI.length - 1; i++)
				readDNItoString += (char)readDNI[i];

			if(searchedDNI.compareTo(readDNItoString) == 0){
				name = sourceRAF.readLine();
				surnames = sourceRAF.readLine();
				return new Employee(readDNItoString, name, surnames);
			}		
		}
		/*
		  Incluso si la posición en el espacio de acceso aleatorio del archivo correspondiente al DNI introducido
		  se encuentra vacía, es necesario realizar una búsqueda secuencial en el área de excedentes, donde todavía
		  podría existir un empleado con el mismo código si, por ejemplo, el que estuviere almacenado en el espacio
		  de acceso aleatorio hubiese sido eliminado con anterioridad.
		*/
		sourceRAF.seek(INIT_FILE_SIZE);

		while(sourceRAF.getFilePointer() < sourceRAF.length()){
			
			readDNItoString = sourceRAF.readLine();
			name = sourceRAF.readLine();
			surnames = sourceRAF.readLine();

			if(searchedDNI.compareTo(readDNItoString) == 0)
				return new Employee(readDNItoString, name, surnames);
		}

		//Llegado este punto, se tiene la certeza de que no existe ninǵun empleado con el DNI especificado.
		return null;
	}



	//Eliminar un empleado a partir de su DNI.
	private static String removeFromRAF(String searchedDNI) throws IOException{

		searchedDNI = Employee.generateNIF(searchedDNI);
		int position = calculatePosition(searchedDNI);
		byte[] readDNI = new byte[10],
			   fileContents;
		String readDNItoString = "",
			   name,
			   surnames,
			   fullName = "";
		File sourceFile = new File(FILE_NAME),
			 targetFile = new File(FILE_NAME + ".tmp");
		RandomAccessFile targetRAF = new RandomAccessFile(targetFile, "rw");

		//Instrucción de posicionamiento.
		sourceRAF.seek(position);
		sourceRAF.read(readDNI);

		//En primer lugar, se comprueba si en la posición calculada existe algún dato.
		if(10 == readDNI[readDNI.length - 1]){

			for(int i=0; i<readDNI.length - 1; i++)
				readDNItoString += (char)readDNI[i];

			//En caso afirmativo, se compara el valor leído en el archivo con la cadena recibida como parámetro.
			if(searchedDNI.compareTo(readDNItoString) == 0){

				fullName = sourceRAF.readLine() + " " + sourceRAF.readLine();
				/*
				  Este array de 'bytes' almacenará, en primera instancia, el contenido del archivo fuente
				  desde el comienzo del mismo hasta llegar al punto en que figura el empleado a eliminar.
				*/
				fileContents = new byte[position];
				sourceRAF.seek(0);
				/*
				  Con el método readFully(), se graba en el array el contenido del archivo desde el cual se
				  invoca en una longitud equivalente a la del propio array, comenzando desde la posición en
				  que se encuentra el puntero del archivo.
				*/
				sourceRAF.readFully(fileContents);
				//targetRAF.seek(0);
				//Acto seguido, el contenido copiado se escribe en el archivo de destino.
				targetRAF.write(fileContents);
				/*
				  En este punto, el archivo de destino contiene toda la información que hay en el de origen desde
				  el byte 0 hasta 'position', donde se localiza el empleado a eliminar. Se trata, por tanto, de
				  desplazar el puntero del archivo fuente, saltando el espacio de un registro (ITEM_SIZE), y
				  repetir las instrucciones anteriores para grabar el resto de la extensión de este archivo en el
				  de destino, donde habrá que tomar también la precacución de dejar desierto el registro correspondiente
				  al empleado indicado.
				*/
				fileContents = new byte[(int)sourceRAF.length() - position - ITEM_SIZE];
				sourceRAF.seek(position + ITEM_SIZE);
				sourceRAF.readFully(fileContents);
				targetRAF.seek(targetRAF.getFilePointer() + ITEM_SIZE);
				//targetRAF.seek(position + ITEM_SIZE); //Para el caso, esta instrucción y la anterior son equivalentes.
				targetRAF.write(fileContents);

				sourceFile.delete(); //El fichero original se elimina del sistema de archivos.
				targetFile.renameTo(sourceFile); //El de destino pasa a denominarse como el de origen.

				return fullName;
			}
		}
		/*
		  La ausencia de datos en el espacio de acceso aleatorio no significa que el empleado no haya sido
		  registrado, ya que todavía podría localizarse en el área de excedentes. Ahora, alcanzado este punto
		  del algoritmo, se tiene la certeza de que el empleado con el DNI especificado sólo podría localizarse
		  en el espacio de sinónimos y que, por tanto, los INIT_FILE_SIZE primeros bytes del archivo fuente han 
		  de reproducirse en el de destino tal cual.
		*/
		fileContents = new byte[INIT_FILE_SIZE];
		sourceRAF.seek(0);
		sourceRAF.readFully(fileContents);
		//targetRAF.seek(0);
		targetRAF.write(fileContents);

		while(sourceRAF.getFilePointer() < sourceRAF.length() && (readDNItoString = sourceRAF.readLine()).compareTo(searchedDNI) != 0)
			targetRAF.writeBytes(readDNItoString + "\n" + sourceRAF.readLine() + "\n" + sourceRAF.readLine() + "\n");

		if(readDNItoString.compareTo(searchedDNI) == 0)
			fullName = sourceRAF.readLine() + " " + sourceRAF.readLine();

		while(sourceRAF.getFilePointer() < sourceRAF.length())
			targetRAF.writeBytes(readDNItoString + "\n" + sourceRAF.readLine() + "\n" + sourceRAF.readLine() + "\n");
		
		sourceFile.delete(); //El fichero original se elimina del sistema de archivos.
		targetFile.renameTo(sourceFile); //El de destino pasa a denominarse como el de origen.

		return fullName;
	}



	//------- Utilidades utilizadas en los métodos anteriores -------

	//Instanciar un nuevo objeto Employee a partir de los datos introducidos por el usuario.
	private static Employee instanceNewEmployee(){

		String dni,
			   name,
			   surnames;

		System.out.println(Format.RESET_FORMAT);
		System.out.println("REGISTRO DE NUEVO EMPLEADO");
		System.out.println("--------------------------");

		System.out.print("DNI: " + Format.CYAN_COLOUR);
		dni = input.next();

		System.out.print(Format.RESET_FORMAT + "Nombre: " + Format.CYAN_COLOUR);
		input.nextLine();
		name = input.nextLine();
		
		System.out.print(Format.RESET_FORMAT + "Apellidos: " + Format.CYAN_COLOUR);
		surnames = input.nextLine();

		System.out.print(Format.RESET_FORMAT);

		return new Employee(dni, name, surnames);
	}


	//Calcular la posición que ocupará un empleado en el fichero RAF a partir de su DNI.
	private static int calculatePosition(String dni){
		return (Integer.parseInt(dni.substring(0,8)) % 1000) * ITEM_SIZE;
	}
}
