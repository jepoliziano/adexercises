//1) Realiza una aplicación que muestre las propiedades del usuario en el sistema.

import java.util.*;


public class Exercise1{
	
	public static void main(String[] args){

		Properties props = System.getProperties();
		Enumeration<Object> directives = props.keys(); //En la instancia 'directives' se almacena el nombre de todas las directivas del sistema.
		String directive,
			   value;

		System.out.println("\nPROPIEDADES DEL USUARIO");
		System.out.println("-----------------------");

		while(directives.hasMoreElements()){

			directive = (String)directives.nextElement(); //El nombre de cada una de las directivas recogidas.

			//Con la siguiente condición, se trata de filtrar todas las directivas que no guarden relación con el usuario.
			if(directive.substring(0,4).compareTo("user") == 0){

				//La cadena 'value' almacena el valor de cada directiva, en ausencia del cual, tomará el de "unset".
				if((value = props.getProperty(directive)).compareTo("") == 0)
					value = "unset";
				
				System.out.println(directive + "=" + value);
			}
		}
		System.out.println();
	}
}