
import java.util.*;



public class User{
	

	//------- Atributos -------

	//Atributos asociados a la instancia
	private String country;
	private String dir;
	private String home;
	private String name;
	private String language;

	//Atributos estáticos de la clase
	private static Properties props;

	static{ props = System.getProperties(); }


	//------- Constructores -------

	//Constructor por defecto
	public User(){
		this.country = props.getProperty("user.country");
		this.dir = props.getProperty("user.dir");
		this.home = props.getProperty("user.home");
		this.name = props.getProperty("user.name");
		this.language = props.getProperty("user.language");
	}

	//Constructor general
	public User(String country, String dir, String home, String name, String language){
		this.country = country;
		this.dir = dir;
		this.home = home;
		this.name = name;
		this.language = language;
	}

	//Constructor de copia
	public User(User user){
		this.country = user.country;
		this.dir = user.dir;
		this.home = user.home;
		this.name = user.name;
		this.language = user.language;
	}


	//------- Métodos -------
	
	//Métodos 'setter' y 'getter'
	public void setCountry(String country){ this.country = country; }
	public String getCountry(){ return this.country; }

	public void setDir(String dir){ this.dir = dir; }
	public String getDir(){ return this.dir; }

	public void setHome(String home){ this.home = home; }
	public String getHome(){ return this.home; }

	public void setName(String country){ this.name = name; }
	public String getName(){ return this.name; }

	public void setLanguage(String language){ this.language = language; }
	public String getLanguage(){ return this.language; }


	//Métodos adicionales

	//Sobrescritura del método toString() de la clase Object
	@Override public String toString(){

		return "\nPaís: " + this.country +
			   "\nDirectorio de trabajo: " + this.dir +
			   "\nDirectorio de inicio de sesión: " + this.home +
			   "\nNombre: " + this.name +
			   "\nIdioma: " + this.language;
	}

	/*
	  El siguiente método devuelve los atributos de la instancia actual como elementos de un array de cadenas
	  con el objetivo de simplificar el código a la hora de crear el archivo XML.
	*/
	public String[] getAttributes(){
		return new String[]{ this.country, this.dir, this.home, this.name, this.language };
	}
}
