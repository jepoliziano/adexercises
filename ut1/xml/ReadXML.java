
import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;
import org.xml.sax.*;



public class ReadXML{
	

	//Atributos estáticos de la clase
	private static String xmlFile; //Nombre del archivo XML

	static{ xmlFile = "users.xml"; }


	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException{

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(xmlFile);

		//El objeto al que apunta 'users' almacenará el conjunto de usuarios definidos en el archivo XML.
		NodeList users = doc.getElementsByTagName("user");
		Node user = null; //Cada uno de los usuarios almacenados en 'users'.
		Element elmnt = null;
		/*
		  Conjunto de atributos con que se identifica a un usuario. Este array tiene el
		  propósito de simplificar el código que sigue.
		*/
		String[] userAttributes = new String[]{"country", "dir", "home", "name", "language"};


		System.out.println("\nUSUARIOS DEL SISTEMA");
		System.out.println("--------------------\n");

		for(int i=0; i<users.getLength(); i++){

			user = users.item(i);
			elmnt = (Element)user;

			/*
			  A través del vector 'userAttributes', cada iteración del bucle a continuación permite
			  obtener el valor de cada uno de los atributos de los usuarios inscritos en el archivo
			  XML sin necesidad de escribir una línea para cada uno (de los atributos).
			*/
			for(int j=0; j<userAttributes.length; j++)
				System.out.println("user." + userAttributes[j] + "=" + elmnt.getElementsByTagName(userAttributes[j]).item(0).getTextContent());
			
			System.out.println();
		}
	}
}
