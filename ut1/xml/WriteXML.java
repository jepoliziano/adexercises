
import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;



public class WriteXML{


	//Atributos estáticos de la clase
	private static Scanner sc;
	private static String xmlFile; //Nombre del archivo XML

	static{
		sc = new Scanner(System.in);
		xmlFile = "users.xml";
	}

	
	public static void main(String[] args) throws ParserConfigurationException, TransformerException{


		ProcessBuilder pb = new ProcessBuilder("clear");
		Process startProcess;
		int input;
		Vector<User> users = new Vector<User>(); //Array dinámico para almacenar los objetos User instanciados.


		System.out.println("\nInfo: partir de los datos introducidos desde teclado, el programa creará un archivo XML con información de usuarios del sistema.");	


		while(true){			
			/*
			  Interacción con el usuario para que decida si desea detallar los datos del objeto User
			  o si, por el contrario, prefiere crear un usuario a partir del constructor por defecto.
			*/
			System.out.print("\nPulse 1 para especificar los datos del usuario a registrar o 2 para crear un usuario por defecto: [1,2] ");
			input = sc.nextInt();

			//Control de errores
			while(input != 1 && input != 2){

				System.out.println("\nError: '" + input + "' no es una entrada válida.");
				System.out.print("\nPulse 1 para especificar los datos del usuario a registrar o 2 para crear un usuario por defecto: [1,2] ");
				input = sc.nextInt();
			}

			/*
			  Invocación al método desde el que se instancian los objetos User y adhesión del
			  valor de retorno al array dinámico.
			*/
			users.add(createUser(input));

			System.out.println("\nInfo: nuevo usuario registrado con éxito.");

			//Interacción con el usuario para preguntarle si desea crear una nueva instancia.
			System.out.print("\n¿Desea registrar un nuevo usuario? [s,n] ");
			input = (int)sc.next().toLowerCase().charAt(0);

			//Control de errores.
			while(input != 110 && input != 115){

				System.out.println("\nError: '" + (char)input + "' no es una entrada válida.");
				System.out.print("\n¿Desea registrar un nuevo usuario? [s,n] ");
				input = (int)sc.next().toLowerCase().charAt(0);
			}

			/*
			  Si el usuario decide no crear una nueva instancia, el bucle se interrumpe y el programa continúa
			  con la creación del archivo XML. En caso contrario, la consola se vacía y se repite el proceso
			  de consulta al usuario.
			*/
			if(input == 110)
				break;

			try{
				startProcess = pb.inheritIO().start();
				startProcess.waitFor();
			
			} catch(Exception e){
				e.printStackTrace();
			}
		}


		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		DOMImplementation dom = db.getDOMImplementation();
		Document doc = dom.createDocument(null, "xml", null);

		Element rootNode = doc.createElement("users"),
				userNode = null,
				dataNode = null;
		Text txt = null;
		/*
		  Array de cadenas, cada uno de cuyos elementos almacena el valor de uno de los
		  atributos de la instancia User.
		*/
		String[] attributeValues;
		/*
		  Conjunto de etiquetas del archivo XML que son descendientes directas del nodo 'user'
		  y que se corresponden con los atributos a almacenar para cada usuario.
		*/
		String[] attributeNames = new String[]{"country", "dir", "home", "name", "language"};

		doc.getDocumentElement().appendChild(rootNode);


		for(User user: users){

			userNode = doc.createElement("user");
			rootNode.appendChild(userNode);
			attributeValues = user.getAttributes();

			for(int i=0; i<attributeValues.length; i++){

				dataNode = doc.createElement(attributeNames[i]);
				userNode.appendChild(dataNode);
				txt = doc.createTextNode(attributeValues[i]);
				dataNode.appendChild(txt);
			}
		}
	

		Source src = new DOMSource(doc);
		Result result = new StreamResult(xmlFile);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(src,result);

		System.out.println("\nInfo: el archivo XML se ha creado con éxito.\n");
	}



	//Método para instanciar usuarios.
	public static User createUser(int input){

		//Atributos a almacenar para cada usuario
		String country = null,
			   dir = null,
			   home = null,
			   name = null,
			   language = null;


		if(input == 2)
			return new User();

		System.out.println("\nINFORMACIÓN DEL USUARIO");
		System.out.println("-----------------------");

		System.out.print("País: ");
		country = sc.next();

		System.out.print("Directorio de trabajo: ");
		dir = sc.next();

		System.out.print("Directorio de inicio de sesión: ");
		home = sc.next();

		System.out.print("Nombre: ");
		name = sc.next();

		System.out.print("Idioma: ");
		language = sc.next();

		return new User(country, dir, home, name, language);
	}
}
