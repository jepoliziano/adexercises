/*
  3) Crear un fichero de propiedades con el siguiente contenido:

	#Fichero de configuración de la aplicación X

	version=1.2.3
	lanzamiento=11/08/2021
	standalone=yes
	port=5858

  Posteriormente, el programa cargará estas propiedades, modificará la fecha y la versión y actualizará el fichero.
*/


import java.util.Properties;
import java.sql.Date;
import java.time.LocalDate;
import java.text.SimpleDateFormat;
import java.io.*;



public class Exercise3{

	public static void main(String[] args) throws FileNotFoundException, IOException{

		Properties props = new Properties();
		String fileName = "app.conf";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = Date.valueOf(LocalDate.now());

		//Propiedades a almacenar.
		props.setProperty("version", "1.2.3");
		props.setProperty("release", sdf.format(date));
		props.setProperty("standalone", "yes");
		props.setProperty("port", "7549");

		//Generación (si no exisitiere) del archivo de configuración con las propiedades especificadas.
		props.store(new FileWriter(fileName), "Archivo de configuración de la app");

		//Salida por consola de las directivas del archivo antes de modificarlo.
		System.out.println("\nARCHIVO DE CONFIGURACIÓN ORIGINAL");
		System.out.println("=================================\n");
		props.load(new FileReader(fileName));
		props.list(System.out);

		//Alteración de las directivas 'release' y 'version'.
		date = Date.valueOf(LocalDate.now().plusMonths((long)7).plusDays((long)7));
		props.setProperty("version", "1.2.4");
		props.setProperty("release", sdf.format(date));

		//Sobrescritura del fichero con los nuevos valores.
		props.store(new FileWriter(fileName), "Archivo de configuración de la app");

		//Salida por consola de las directivas del archivo de configuración modificado
		System.out.println("\n\nARCHIVO DE CONFIGURACIÓN MODIFICADO");
		System.out.println("===================================\n");
		props.load(new FileReader(fileName));
		props.list(System.out);
		System.out.println();
	}
}
